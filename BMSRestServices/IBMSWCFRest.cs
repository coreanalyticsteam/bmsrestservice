﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace BMSRestServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBMSWCFRest" in both code and config file together.
    [ServiceContract]
    public interface IBMSWCFRest
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/UpdateBookingStatus")]
        string UpdateBookingStatus(PolicyContract PolicyDetails); 
        //string UpdateBookingStatus(long leadId, string PolicyNumber, string ProposalNumber, bool IsSTP, string PolicyLink, bool IsLinkPB);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/SetCallBackInBMS")]
        string SetCallBackInBMS(SetCallBackContract SetCallBackDetails);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/UpdatePolicyStageStatus")]
        string UpdatePolicyStageStatus(PolicyStageContract PolicyStage);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/CustomerCommResponseAction")]
        string CustomerResponseAction(CustomerResponseAction CustomerResponse);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/GetDocumentDetails?LeadID={LeadID}&CustomerID={CustomerID}&Type={Type}")]
        DocumentOptionByCategoryDetails GetDocumentDetails(long LeadID, long CustomerID, short Type);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/UpdateDocumentDetails")]
        string UpdateDocumentDetails(CustomerBookingDocument CustomerBooking);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/UploadFileDetails")]
        string UploadFileDetails(UploadDocumentData UploadDocument);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/UpdateBooking_ITZCASH?ItemId={ItemId}&OrderNo={OrderNo}&Status={Status}")]
        string UpdateBooking_ITZCASH(long ItemId, string OrderNo, string Status);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/GetCustomerSIDetails?ReferenceNo={ReferenceNo}&MobileNo={MobileNo}")]
        CustomerSIDetails GetCustomerSIDetails(string ReferenceNo, string MobileNo);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/InsertPCDDetails")]
        string InsertPCDDetails(PCDData PCDData);
    }
}
