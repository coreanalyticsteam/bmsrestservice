﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
//BMS-342: Added By Sumit
using System.IO;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.Net;
//BMS-342: Added By Sumit

namespace BMSRestServices
{

    [DataContract]
    public class M_PolicyDetails
    {
        #region Instance Properties

        [DataMember]
        public Int32? CustPolicyID { get; set; }

        [DataMember]
        public Int32? SupplierId { get; set; }

        [DataMember]
        public Int32? PlanId { get; set; }

        [DataMember]
        public Decimal? SumInsured { get; set; }

        [DataMember]
        public Decimal? Premium { get; set; }

        [DataMember]
        public Int32? PolicyTerm { get; set; }

        [DataMember]
        public Int32? PremiumPayingTerm { get; set; }

        [DataMember]
        public Int32? PaymentFrequency { get; set; }

        [DataMember]
        public String PolicyNumber { get; set; }

        [DataMember]
        public Int32? GracePeriod { get; set; }

        [DataMember]
        public string PolicyStartDate { get; set; }

        [DataMember]
        public string PolicyEndDate { get; set; }

        [DataMember]
        public Int32? TPA { get; set; }

        [DataMember]
        public String PreviousPolicyNumber { get; set; }

        [DataMember]
        public Decimal? MinimumDeathBenefit { get; set; }

        [DataMember]
        public Decimal? MaturityBenefit { get; set; }

        [DataMember]
        public Int32? CustomerId { get; set; }

        [DataMember]
        public Int32? BookingRefID { get; set; }

        [DataMember]
        public String DocumentURL { get; set; }

        [DataMember]
        public String ProposalNumber { get; set; }

        [DataMember]
        public Int32? ProductID { get; set; }

        [DataMember]
        public Boolean? IsAddOn { get; set; }

        [DataMember]
        public Int32? ParentPolicyID { get; set; }

        [DataMember]
        public string CreatedOn { get; set; }

        [DataMember]
        public string UpdatedOn { get; set; }

        [DataMember]
        public Int32? CreatedBy { get; set; }

        [DataMember]
        public Int32? UpdatedBy { get; set; }

        [DataMember]
        public String CommAddress { get; set; }

        [DataMember]
        public Boolean? IsSTP { get; set; }

        [DataMember]
        public String CoverType { get; set; }

        [DataMember]
        public string IssuanceDate { get; set; }

        [DataMember]
        public Boolean? ISActive { get; set; }

        [DataMember]
        public String CommPinCode { get; set; }

        [DataMember]
        public Int32? CommCityID { get; set; }

        [DataMember]
        public Int32? CommStateID { get; set; }

        [DataMember]
        public String PermAddress { get; set; }

        [DataMember]
        public String PermPinCode { get; set; }

        [DataMember]
        public Int32? PermCityID { get; set; }

        [DataMember]
        public Int32? PermStateID { get; set; }

        [DataMember]
        public Int32? LoadingPercentage { get; set; }

        [DataMember]
        public Decimal? LoadingPremium { get; set; }

        [DataMember]
        public Int32? PremiumWaivedOff { get; set; }

        [DataMember]
        public Int32? CopaymentPercentage { get; set; }

        #endregion Instance Properties
    }
    [DataContract]
    public class M_PolicyInsuredDetails
    {
        #region Instance Properties

        [DataMember]
        public Int32? PolicyInsuredID { get; set; }

        [DataMember]
        public Int32? CustPolicyID { get; set; }

        [DataMember]
        public Int32? CustMemId { get; set; }

        [DataMember]
        public Boolean? IsPrimary { get; set; }

        [DataMember]
        public string CreatedOn { get; set; }

        [DataMember]
        public string UpdatedOn { get; set; }

        [DataMember]
        public Int32? CreatedBy { get; set; }

        [DataMember]
        public Int32? UpdatedBy { get; set; }

        [DataMember]
        public Boolean? IsActive { get; set; }

        [DataMember]
        public String InsuredName { get; set; }

        [DataMember]
        public string DOB { get; set; }

        [DataMember]
        public Int32? Gender { get; set; }

        [DataMember]
        public Int32? IncomeGroupID { get; set; }

        [DataMember]
        public Int32? MaritalStatusID { get; set; }

        [DataMember]
        public Int32? RelationWithProposerID { get; set; }

        [DataMember]
        public String Title { get; set; }

        [DataMember]
        public Int32? OccupationId { get; set; }

        [DataMember]
        public String Email { get; set; }

        [DataMember]
        public Int64? MobileNo { get; set; }

        #endregion Instance Properties
    }
    [DataContract]
    public class M_PolicyNomineeDetails
    {
        #region Instance Properties

        [DataMember]
        public Int32? PolicyNomineeID { get; set; }

        [DataMember]
        public Int32? CustPolicyID { get; set; }

        [DataMember]
        public Int32? NomineeMemberID { get; set; }

        [DataMember]
        public Int32? AppointeeMemberID { get; set; }

        [DataMember]
        public Int32? PercentageShare { get; set; }

        [DataMember]
        public string CreatedOn { get; set; }

        [DataMember]
        public string UpdatedOn { get; set; }

        [DataMember]
        public Int32? CreatedBy { get; set; }

        [DataMember]
        public Int32? UpdatedBy { get; set; }

        [DataMember]
        public Int32? RelationshipTypeID { get; set; }

        [DataMember]
        public Int32? NomineeAge { get; set; }

        [DataMember]
        public String NomineeRelationship { get; set; }

        [DataMember]
        public Int32? AppointeeAge { get; set; }

        [DataMember]
        public String AppointeeRelationship { get; set; }

        [DataMember]
        public String NomineeName { get; set; }

        [DataMember]
        public String AppointeeName { get; set; }

        [DataMember]
        public String NomineeGender { get; set; }

        #endregion Instance Properties
    }
    [DataContract]
    public class M_VehicleDetails
    {
        #region Instance Properties

        [DataMember]
        public Int64? CustVehicleID { get; set; }

        [DataMember]
        public Int32? ProductID { get; set; }

        [DataMember]
        public Int64? VehicleCode { get; set; }

        [DataMember]
        public String ChassisNo { get; set; }

        [DataMember]
        public String EngineNo { get; set; }

        [DataMember]
        public String RegistrationNo { get; set; }

        [DataMember]
        public string ExpiryDate { get; set; }

        [DataMember]
        public Int32? CurrentInsurerId { get; set; }

        [DataMember]
        public string ManufacturingDate { get; set; }

        [DataMember]
        public Int32? CustomerId { get; set; }

        [DataMember]
        public Int32? VariantId { get; set; }

        [DataMember]
        public Int32? ModelId { get; set; }

        [DataMember]
        public Int32? MakeId { get; set; }

        [DataMember]
        public Int64? CustomerAddressId { get; set; }

        [DataMember]
        public Int32? CustPolicyID { get; set; }

        [DataMember]
        public Int32? VehilceTypeId { get; set; }

        [DataMember]
        public string RegistrationDate { get; set; }

        [DataMember]
        public Int32? FuelTypeId { get; set; }

        [DataMember]
        public Int32? CubicCapacityId { get; set; }

        [DataMember]
        public Int32? SeatingCapacity { get; set; }

        [DataMember]
        public Decimal? VehicleIDV { get; set; }

        [DataMember]
        public Decimal? ElectricalAccessoriesIDV { get; set; }

        [DataMember]
        public Decimal? NonElectricalAccessoriesIDV { get; set; }

        [DataMember]
        public Decimal? CNGLPGUnitIDV { get; set; }

        [DataMember]
        public Decimal? TotalIDV { get; set; }

        [DataMember]
        public string CreateDate { get; set; }

        [DataMember]
        public string UpdateDate { get; set; }

        [DataMember]
        public Int32? CreatedBy { get; set; }

        [DataMember]
        public Int32? UpdatedBy { get; set; }

        [DataMember]
        public String RegAddress { get; set; }

        [DataMember]
        public Int32? RegisteredStateId { get; set; }

        [DataMember]
        public Int32? RegisteredCityId { get; set; }

        [DataMember]
        public String RegistrationCode { get; set; }

        [DataMember]
        public String RegistrationRTOCode { get; set; }

        [DataMember]
        public Int32? RegistrationPostOfficeVORef { get; set; }

        [DataMember]
        public String RegistrationPostCodeLocality { get; set; }

        [DataMember]
        public String PreviousInsurerAddress { get; set; }

        #endregion Instance Properties
    }
    [DataContract]
    public class M_AdditionalVehicleDetails
    {
        #region Instance Properties

        [DataMember]
        public Int64? CustAddVehicleID { get; set; }

        [DataMember]
        public Int64? CustVehicleID { get; set; }

        [DataMember]
        public String RegisteredOwnerAddress { get; set; }

        [DataMember]
        public String RegistrationPostCode { get; set; }

        [DataMember]
        public String RegistrationContactNo { get; set; }

        [DataMember]
        public Int32? PreviousInsurerId { get; set; }

        [DataMember]
        public String VehicleOwnedBy { get; set; }

        [DataMember]
        public String OrganizationName { get; set; }

        [DataMember]
        public String ContactPersonInOrganization { get; set; }

        [DataMember]
        public Boolean? IsVehicleRegisteredOwner { get; set; }

        [DataMember]
        public Boolean? IsVehicleBoughtWithinLast12Months { get; set; }

        [DataMember]
        public Boolean? IsRCEndorsedInCurrentOwnerName { get; set; }

        [DataMember]
        public String RCOwnerName { get; set; }

        [DataMember]
        public String CarUsedFor { get; set; }

        [DataMember]
        public Boolean? IsClaimsMadeInPreviousPolicy { get; set; }

        [DataMember]
        public Int32? NotClaimedSince { get; set; }

        [DataMember]
        public string RegisteredOwnerDOB { get; set; }

        [DataMember]
        public Int32? VoluntaryExcess { get; set; }

        [DataMember]
        public Int32? ProfessionId { get; set; }

        [DataMember]
        public Boolean? IsAutomobileAssociationMember { get; set; }

        [DataMember]
        public String AutomobileAssociationName { get; set; }

        [DataMember]
        public String AutomobileAssociationMembershipNo { get; set; }

        [DataMember]
        public string AutomobileAssociationMembershipExpiryDate { get; set; }

        [DataMember]
        public Boolean? IsAntiTheftDeviceApprovedByARAI { get; set; }

        [DataMember]
        public Boolean? IsAvailAgeDiscount { get; set; }

        [DataMember]
        public String LoanType { get; set; }

        [DataMember]
        public String FinancialInstitutionName { get; set; }

        [DataMember]
        public String FinancialInstitutionCode { get; set; }

        [DataMember]
        public String FinancialInstitutionAddress { get; set; }

        [DataMember]
        public String FinancialInstitutionCity { get; set; }

        [DataMember]
        public String VehicleDrivenBy { get; set; }

        [DataMember]
        public String DriverFullName { get; set; }

        [DataMember]
        public Int32? DrivingExperience { get; set; }

        [DataMember]
        public string DriverDateOfBirth { get; set; }

        [DataMember]
        public Int32? DriverAge { get; set; }

        [DataMember]
        public Int32? TotalDrivers { get; set; }

        [DataMember]
        public String DriverGender { get; set; }

        [DataMember]
        public String ParkingType { get; set; }

        [DataMember]
        public String DriverMaritalStatus { get; set; }

        [DataMember]
        public String AnnualKilometersRuns { get; set; }

        [DataMember]
        public Int32? TotalNumberOfClaimsMade { get; set; }

        [DataMember]
        public String NCBDocumentHolds { get; set; }

        [DataMember]
        public String LicenseType { get; set; }

        [DataMember]
        public Int32? LicenseAge { get; set; }

        [DataMember]
        public Boolean? IsEffectiveDL { get; set; }

        [DataMember]
        public Boolean? IsUnderLoan { get; set; }

        [DataMember]
        public String VehicleMostlyUsed { get; set; }

        [DataMember]
        public String LicenseNumber { get; set; }

        [DataMember]
        public string LicenseIssueDate { get; set; }

        [DataMember]
        public string LicenseExpDate { get; set; }

        [DataMember]
        public String LicenseIssuingAuthority { get; set; }

        [DataMember]
        public String VehicleColor { get; set; }

        [DataMember]
        public Boolean? IsElectricalAccessories { get; set; }

        [DataMember]
        public string ElectricalAccessoriesInvoiceDate { get; set; }

        [DataMember]
        public Boolean? IsNonElectricalAccessories { get; set; }

        [DataMember]
        public string NonElectricalAccessoriesInvoiceDate { get; set; }

        [DataMember]
        public Boolean? IsCNGFitted { get; set; }

        [DataMember]
        public String TypeOfCNGKit { get; set; }

        [DataMember]
        public string CNGInvoiceDate { get; set; }

        [DataMember]
        public Boolean? IsPACoverForUnnamedPassengers { get; set; }

        [DataMember]
        public Decimal? PACoverForUnnamedPassengers { get; set; }

        [DataMember]
        public Boolean? IsInsurePaidDriver { get; set; }

        [DataMember]
        public Decimal? PaidDriverLiabilityAmount { get; set; }

        [DataMember]
        public string CreateDate { get; set; }

        [DataMember]
        public string UpdateDate { get; set; }

        [DataMember]
        public Int32? CreatedBy { get; set; }

        [DataMember]
        public Int32? UpdatedBy { get; set; }

        [DataMember]
        public Int32? ClaimAmount { get; set; }

        [DataMember]
        public Boolean? IsPrePolicyHaveZeroDep { get; set; }

        [DataMember]
        public Decimal? ValueOfElectricalAccessories { get; set; }

        [DataMember]
        public Decimal? ValueOfNonElectricalAccessories { get; set; }

        [DataMember]
        public Decimal? CNGAmount { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string PACoverForOwnerDriver { get; set; }

        #endregion Instance Properties
    }
    [DataContract]
    public class M_AdditionalCoverItemVehicleDetails
    {
        #region Instance Properties

        [DataMember]
        public Int64? CustAddCoverVehicleID { get; set; }

        [DataMember]
        public Int64? CustVehicleID { get; set; }

        [DataMember]
        public Int32? ItemType { get; set; }

        [DataMember]
        public String ItemName { get; set; }

        [DataMember]
        public String ItemMakeModel { get; set; }

        [DataMember]
        public Int32? ItemMfgYear { get; set; }

        [DataMember]
        public Decimal? ItemAmount { get; set; }

        [DataMember]
        public string CreateDate { get; set; }

        [DataMember]
        public string UpdateDate { get; set; }

        [DataMember]
        public Int32? CreatedBy { get; set; }

        [DataMember]
        public Int32? UpdatedBy { get; set; }

        #endregion Instance Properties
    }
    [DataContract]
    public class M_PolicyInsuredAdditionalInfo
    {
        #region Instance Properties

        [DataMember]
        public Int32? InsAddInfoID { get; set; }

        [DataMember]
        public Int32? PolicyInsuredID { get; set; }

        [DataMember]
        public String PANNum { get; set; }

        [DataMember]
        public String DLNum { get; set; }

        [DataMember]
        public String Passport { get; set; }

        [DataMember]
        public Int32? Height { get; set; }

        [DataMember]
        public Int32? Weight { get; set; }

        [DataMember]
        public Boolean? ChewsTobaccoORSmokes { get; set; }

        [DataMember]
        public String AnyPreExistingDisease { get; set; }

        [DataMember]
        public Boolean? AlcoholORDrugAbuse { get; set; }

        [DataMember]
        public String AnyHositalizationHistory { get; set; }

        [DataMember]
        public Int32? PostOfficeVORef { get; set; }

        [DataMember]
        public String PostCodeLocality { get; set; }

        [DataMember]
        public Boolean? IsRegAddSameAsCommAddress { get; set; }

        [DataMember]
        public Int32? OccupationID { get; set; }

        [DataMember]
        public string CreatedOn { get; set; }

        [DataMember]
        public string UpdatedOn { get; set; }

        [DataMember]
        public Int32? CreatedBy { get; set; }

        [DataMember]
        public Int32? UpdatedBy { get; set; }

        [DataMember]
        public String InsuredExclusionReason { get; set; }

        [DataMember]
        public String DiseaseExcluded { get; set; }

        [DataMember]
        public Int32? DiseaseExcludedType { get; set; }

        [DataMember]
        public Int32? DiseaseExcludedWaitingPeriod { get; set; }

        #endregion Instance Properties
    }

    [DataContract]
    public class PCDData
    {
        [DataMember (EmitDefaultValue=false)]
        public int leadId { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public M_PolicyNomineeDetails[] NomineeDetails { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public M_VehicleDetails VehicleDetails { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public M_AdditionalVehicleDetails AddVehicleDetails { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public M_PolicyDetails PolicyDetails { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public M_AdditionalCoverItemVehicleDetails[] AdditionalCoverItemVehicleDetails { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public M_PolicyInsuredAdditionalInfo[] AddInsuredDetails { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public M_PolicyInsuredDetails[] InsuredDetails { get; set; }
    }

    [DataContract]
    public class PolicyContract
    {
        [DataMember]
        public string LeadId { get; set; }

        [DataMember]
        public string PolicyNumber { get; set; }

        [DataMember]
        public string ProposalNumber { get; set; }

        [DataMember]
        public bool IsSTP { get; set; }

        [DataMember]
        public string PolicyLink { get; set; }

        [DataMember]
        public string IsLinkPB { get; set; }
    }

    [DataContract]
    public class SetCallBackContract
    {
        [DataMember]
        public Int64 LeadId { get; set; }

        [DataMember]
        public Int32 AgentID { get; set; }

        [DataMember]
        public Int32 @MinutesToAdd { get; set; }
    }

    
    [DataContract]
    public class PolicyStageContract
    {
        [DataMember]
        public string LeadId { get; set; }

        [DataMember]
        public string PolicyNumber { get; set; }

        [DataMember]
        public string ProposalNumber { get; set; }

        [DataMember]
        public string IsSTP { get; set; }

        [DataMember]
        public string PolicyLink { get; set; }

        [DataMember]
        public string IsLinkPB { get; set; }

        [DataMember]
        public short SupplierID { get; set; }

        [DataMember]
        public short ProductID { get; set; }
    }

    [DataContract]
    public class CustomerResponseAction
    {
        [DataMember]
        public Int64 LeadId { get; set; }

        [DataMember]
        public Int32 AgentID { get; set; }

        [DataMember]
        public Int32 ActionType { get; set; }

        [DataMember]
        public string CommType { get; set; }

        [DataMember]
        public string TriggerName { get; set; }
    }

    [DataContract]
    public class UploadDocumentData
    {
        [DataMember]
        public Int32 CustomerID { get; set; }

        [DataMember]
        public Int32 LeadID { get; set; }

        [DataMember]
        public Int16 ProductID { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public Int16 UserID { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public string UploadMode { get; set; }

        [DataMember]
        public Int16 Flag { get; set; }

        [DataMember]
        public string FileStream { get; set; }
    }

    [DataContract]
    public class CustomerBookingDocument
    {
        [DataMember]
        public Int32 CustomerID { get; set; }

        [DataMember]
        public Int32 LeadID { get; set; }

        [DataMember]
        public Int16 DocCategoryID { get; set; }

        [DataMember]
        public Int16 DocumentID { get; set; }

        [DataMember]
        public Int32 ParentCustDocumentID { get; set; }

        [DataMember]
        public string HostName { get; set; }

        [DataMember]
        public string DocumentURL { get; set; }

        [DataMember]
        public Int16 ProductID { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public Int16 UserID { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public string UploadMode { get; set; }

        [DataMember]
        public Int16 Flag { get; set; }

        [DataMember]
        public string FileStream { get; set; }
    }

    [DataContract]
    public class DocumentURLDetails
    {
        [DataMember]
        public Int32 CustDocumentID { get; set; }
        [DataMember]
        public string DocumentUrl { get; set; }
        [DataMember]
        public string Document { get; set; }
        [DataMember]
        public Int16 DocCategoryID { get; set; }
        [DataMember]
        public string HostName { get; set; }
    }

    [DataContract]
    public class DocumentDetails
    {
        [DataMember]
        public Int16 DocumentID { get; set; }

        [DataMember]
        public string Document { get; set; }

        [DataMember]
        public string DocCategoryName { get; set; }

        [DataMember]
        public Int16 DocCategoryID { get; set; }

    }

    [DataContract]
    public class DocumentCategoryDetails
    {
        [DataMember]
        public List<DocumentDetails> MasterDocument { get; set; }

        [DataMember]
        public List<DocumentURLDetails> LeadDocument { get; set; }

        [DataMember]
        public List<DocumentURLDetails> CustomerDocument { get; set; }

        [DataMember]
        public string DocCategoryName { get; set; }

        [DataMember]
        public Int16 DocCategoryID { get; set; }

    }

    [DataContract]
    public class DocumentOptionByCategoryDetails
    {
        [DataMember]
        public List<DocumentCategoryDetails> docOptionsByCategory { get; set; }
        [DataMember]
        public int PlanId { get; set; }
        [DataMember]
        public int ProductId { get; set; }
        [DataMember]
        public int SupplierId { get; set; }
        [DataMember]
        public string PlanName { get; set; }
        [DataMember]
        public string ProductName { get; set; }
        [DataMember]
        public string SupplierName { get; set; }
        [DataMember]
        public long LeadID { get; set; }
        [DataMember]
        public long CustomerID { get; set; }

    }

    [DataContract]
    public class CustomerSIDetails
    {
        [DataMember]
        public string CustomerName { get; set; }
        [DataMember]
        public string PolicyIssuanceStatus { get; set; }
        [DataMember]
        public string PlanName { get; set; }
        [DataMember]
        public string EmailId { get; set; }
        [DataMember]
        public string PolicyIssuanceDate { get; set; }
        [DataMember]
        public string PolicyTerm { get; set; }
        [DataMember]
        public string PaymentPeriodicity { get; set; }
        [DataMember]
        public string InstallmentsPaid { get; set; }

    }

    public class ServerFileUpload
    {
        public static bool IsFTPWorking()
        {
            return true;
        }

        public static bool FtpDirectoryExists(string directoryPath, string ftpUser, string ftpPassword)
        {
            bool IsExists = true;
            try
            {
                if (!Directory.Exists(directoryPath))
                    IsExists = false;
            }
            catch (Exception ex)
            {

                IsExists = false;
            }
            return IsExists;
        }


        public static bool UploadFileToFTP(Stream file, string MasterServerPath, string FileName)
        {

            string[] SubDirectories;
            string NewMasterServerPath = string.Empty;
            string userName = string.Empty, password = string.Empty;
            //SubDirectories = MasterServerPath.Split('/');
            //NewMasterServerPath = AppDomain.CurrentDomain.BaseDirectory + "\\" + Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FtpDocPath"]);
            NewMasterServerPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FtpDocPath"]);
            SubDirectories = MasterServerPath.Replace(NewMasterServerPath, "").Split('/');
            //SubDirectories = MasterServerPath.Replace(NewMasterServerPath, "").Split('\\');

            for (int i = 1; i < SubDirectories.Length; i++)
            {
                NewMasterServerPath += "/" + SubDirectories[i] + "/";
                //NewMasterServerPath += "\\" + SubDirectories[i] + "\\";
                if (!FtpDirectoryExists(NewMasterServerPath, userName, password))
                    FtpMakeDirectory(NewMasterServerPath, userName, password);
            }

            if ((FtpDirectoryExists(NewMasterServerPath, userName, password)))
            {
                FileStream targetStream = null;
                try
                {
                    string filePath = NewMasterServerPath + "/" + FileName;

                    using (targetStream = new FileStream(filePath, FileMode.Create,
                                          FileAccess.Write, FileShare.None))
                    {
                        //read from the input stream in 65000 byte chunks

                        const int bufferLen = 65000;
                        byte[] buffer = new byte[bufferLen];
                        int count = 0;
                        while ((count = file.Read(buffer, 0, bufferLen)) > 0)
                        {
                            // save to output stream
                            targetStream.Write(buffer, 0, count);
                        }
                        targetStream.Close();
                        file.Close();
                    }
                    return true;
                }
                catch (Exception ex)
                {

                    return false;
                }
            }
            return false;
        }

        public static bool FtpFileExists(string directoryPath, string ftpUser, string ftpPassword)
        {
            bool IsExists = true;
            try
            {
                if (!File.Exists(directoryPath))
                    IsExists = false;
            }
            catch (Exception ex)
            {

                IsExists = false;
            }
            return IsExists;
        }

        public static bool FtpMakeDirectory(string MasterServerPath, string ftpUser, string ftpPassword)
        {
            bool IsExists = true;
            try
            {
                Directory.CreateDirectory(MasterServerPath);
            }
            catch (Exception ex)
            {
                IsExists = false;

            }
            return IsExists;
        }



        public static bool DownloadFileFromFtp(string MasterServerPath, string fileName)
        {
            try
            {
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.TransmitFile(MasterServerPath);
                HttpContext.Current.Response.Flush();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Stream GetStreamFromFtp(string MasterServerPath)
        {
            byte[] buff = null;
            try
            {
                FileStream fs = new FileStream(MasterServerPath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                long numBytes = new FileInfo(MasterServerPath).Length;
                buff = br.ReadBytes((int)numBytes);
                Stream sr = new MemoryStream(buff);
                return sr;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                buff = null;
            }
        }

    }

    public class BMSWCFRest : IBMSWCFRest
    {

        public string UpdateBookingStatus(PolicyContract PolicyDetails)
        {
            string Message = string.Empty;
            string Exc = string.Empty;
            try
            {
                string json = JsonConvert.SerializeObject(PolicyDetails);
                BusinessLogic bl = new BusinessLogic();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
                sqlQuery.Append(File.ReadAllText(@"D:\BMSRestfulService\InsertUpdateLeadStatusByAPI.txt"));
                //sqlQuery.Append(File.ReadAllText(@"C:\Users\lalan\Desktop\BMS_QA_Post_July15\BMSRestServices_Health_CallTransfer\BMSRestServices\BMSRestServices\InsertUpdateLeadStatusByAPI.txt"));
                sqlQuery.Replace("@LeadIDValue", Convert.ToString(PolicyDetails.LeadId));
                sqlQuery.Replace("@PolicyNoValue", PolicyDetails.PolicyNumber);
                sqlQuery.Replace("@ProposalNoValue", PolicyDetails.ProposalNumber);
                sqlQuery.Replace("@IsSTPValue", Convert.ToInt16(PolicyDetails.IsSTP).ToString());
                sqlQuery.Replace("@PolicyLinkValue", PolicyDetails.PolicyLink);
                sqlQuery.Replace("@IsLinkPBValue", Convert.ToInt16(PolicyDetails.IsLinkPB).ToString());
                DbCommand dbCommand = db.GetSqlStringCommand(sqlQuery.ToString());
                DataSet ds = db.ExecuteDataSet(dbCommand);

                if (ds.Tables.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    Message = Convert.ToString(dr["Message"]);
                    if (Message == "Details Updated")
                        return Message;
                    if (Message != "Status Not Moved")
                    {
                        bl.SendEmailSms(Convert.ToInt64(PolicyDetails.LeadId), Convert.ToString(dr["MobileNo"]), Convert.ToString(dr["EmailID"]), Convert.ToString(dr["StatusID"]), Convert.ToString(dr["SubStatusID"]), Convert.ToString(dr["BookingType"]), Convert.ToString(dr["SupplierId"]));
                    }
                    else
                    {
                        Message = "Status Not Moved";
                    }
                }
                return Message;
            }

            catch (Exception ex) {Exc=ex.Message.ToString(); return ex.Message.ToString(); }
            finally {
                LoggingHelper.LoggingHelper.LogIntoFile(JsonConvert.SerializeObject(PolicyDetails), JsonConvert.SerializeObject(Message), Exc, "UpdateBookingStatus");
            }
        }

        public string SetCallBackInBMS(SetCallBackContract SetCallBackDetails)
        {
            string Message = string.Empty;
            string Exc = string.Empty;
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("[BMS].[Set_AgentServiceRequestCallBackCallTransfer]");
                db.AddInParameter(dbCommand, "@BookingId", DbType.Int64, SetCallBackDetails.LeadId);
                db.AddInParameter(dbCommand, "@AgentID", DbType.Int32, SetCallBackDetails.AgentID);
                db.AddInParameter(dbCommand, "@MinutesToAdd", DbType.Int32, 15);
                DataSet ds = db.ExecuteDataSet(dbCommand);

                if (ds.Tables.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    Message = Convert.ToString(dr["Message"]);
                }
                else
                {
                    Message = "Call Back Not Set: Error code-0";
                }
                dbCommand.Dispose();
                return Message;
            }
            catch (Exception ex) { Exc = ex.Message.ToString(); return ex.Message.ToString(); }
            finally
            {
                LoggingHelper.LoggingHelper.LogIntoFile(JsonConvert.SerializeObject(SetCallBackDetails), JsonConvert.SerializeObject(Message), Exc, "SetCallBackInBMS");
            }
        }

        public string UpdatePolicyStageStatus(PolicyStageContract PolicyStage)
        {
            string Message = "Status Moved";
            string Exc = string.Empty;
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("[BMS].[InsertBookingPolicyStageDetails]");
                db.AddInParameter(dbCommand, "@LeadID", DbType.Int64, PolicyStage.LeadId);
                db.AddInParameter(dbCommand, "@PolicyNo", DbType.String, PolicyStage.PolicyNumber);
                db.AddInParameter(dbCommand, "@ProposalNo", DbType.String, PolicyStage.ProposalNumber);
                if (PolicyStage.IsSTP == "")
                    db.AddInParameter(dbCommand, "@IsSTP", DbType.Boolean, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@IsSTP", DbType.Boolean, Convert.ToBoolean(Convert.ToInt16(PolicyStage.IsSTP)));
                db.AddInParameter(dbCommand, "@PolicyLink", DbType.String, PolicyStage.PolicyLink);
                db.AddInParameter(dbCommand, "@IsLinkPB", DbType.String, PolicyStage.IsLinkPB);
                db.AddInParameter(dbCommand, "@SupplierID", DbType.Int16, PolicyStage.SupplierID);
                db.AddInParameter(dbCommand, "@ProductID", DbType.Int16, PolicyStage.ProductID);
                db.AddInParameter(dbCommand, "@InsertType", DbType.String, "Auto");
                int rowsAffected = db.ExecuteNonQuery(dbCommand);
                if (rowsAffected == 1)
                    Message = "Status Moved";
                return Message;
            }

            catch (Exception ex) { Exc = ex.Message.ToString(); return ex.Message.ToString(); }
            finally
            {
                LoggingHelper.LoggingHelper.LogIntoFile(JsonConvert.SerializeObject(PolicyStage), JsonConvert.SerializeObject(Message), Exc, "UpdatePolicyStageStatus");
            }
        }

        public string CustomerResponseAction(CustomerResponseAction CustomerResponse)
        {
            string Message = string.Empty;
            string Exc = string.Empty;
            try
            {
                int res = 0;
                int AllLead = 0;

                int nCommType = 0;
                if (CustomerResponse.CommType.ToUpper() == "EMAIL")
                    nCommType = 24;
                else if (CustomerResponse.CommType.ToUpper() == "SMS")
                    nCommType = 25;
                else if (CustomerResponse.CommType.ToUpper() == "CALL")
                {
                    nCommType = 1;
                    AllLead = 1;
                }

                if ((CustomerResponse.ActionType == 2 && nCommType != 0) || CustomerResponse.ActionType == 1)
                {

                    
                    if (CustomerResponse.TriggerName != null && CustomerResponse.TriggerName.Contains("Reachable")) { AllLead = 1; }
                    Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                    DbCommand dbCommand = db.GetStoredProcCommand("[BMS].[CustResponseAction]");
                    db.AddInParameter(dbCommand, "@LeadID", DbType.Int64, CustomerResponse.LeadId);
                    db.AddInParameter(dbCommand, "@AgentID", DbType.Int32, CustomerResponse.AgentID);
                    db.AddInParameter(dbCommand, "@ActionType", DbType.Int32, CustomerResponse.ActionType);
                    db.AddInParameter(dbCommand, "@CommType", DbType.Int32, nCommType);
                    db.AddInParameter(dbCommand, "@AllLead", DbType.Int32, AllLead);
                    db.AddParameter(dbCommand, "@Result", DbType.Int32, ParameterDirection.Output, "@Result", DataRowVersion.Default, null);
                    res = db.ExecuteNonQuery(dbCommand);
                    int result = (int)db.GetParameterValue(dbCommand, "@Result");
                    switch (result)
                    {
                        case 1:
                            Message = "Callback set.";
                            break;
                        case 2:
                            Message = "Eventtype changed and CommunicationDetails updated.";
                            break;
                        case 3:
                            Message = "Eventtype changed and CommunicationDetails updated.";
                            break;
                        case 4:
                            Message = "Eventtype changed and CommunicationDetails updated.";
                            break;
                        default:
                            Message = "";
                            break;
                    }
                }

                return Message;
            }
            catch (Exception ex) { Exc = ex.Message.ToString(); return ex.Message.ToString(); }
            finally
            {
                LoggingHelper.LoggingHelper.LogIntoFile(JsonConvert.SerializeObject(CustomerResponse), JsonConvert.SerializeObject(Message), Exc, "CustomerResponseAction");
            }
        }

        public string UpdateDocumentDetails(CustomerBookingDocument CustomerBooking)
        {
            string Message = string.Empty;
            string Exc = string.Empty;


            try
            {
                if (CustomerBooking.ParentCustDocumentID == 0)
                {
                    byte[] data = System.Convert.FromBase64String(CustomerBooking.FileStream);
                    MemoryStream ms = new MemoryStream(data);
                    string extPath = Path.GetExtension(CustomerBooking.FileName).ToLower();
                    if (CustomerBooking.UploadMode == "FTP")
                    {
                        StringBuilder MasterFolderPath = new StringBuilder(System.Configuration.ConfigurationManager.AppSettings["CustomerSpecificDocPath"].ToString());
                        MasterFolderPath.Replace("@CustomerID", Convert.ToString(CustomerBooking.CustomerID));
                        MasterFolderPath.Replace("@LeadId", Convert.ToString(CustomerBooking.LeadID));
                        MasterFolderPath.Replace("@Year", Convert.ToString(DateTime.Now.Year));
                        MasterFolderPath.Replace("@ProductName", Convert.ToString(CustomerBooking.ProductName));
                        if (ServerFileUpload.UploadFileToFTP(ms, Convert.ToString(MasterFolderPath), CustomerBooking.FileName))
                        {
                            CustomerBooking.DocumentURL = MasterFolderPath + "/" + CustomerBooking.FileName;
                        }
                    }
                    else if (CustomerBooking.UploadMode == "Mongo")
                    {
                        var CustID = Convert.ToString(CustomerBooking.CustomerID);
                        var ProdID = Convert.ToString(CustomerBooking.ProductID);
                        var BookID = Convert.ToString(CustomerBooking.LeadID);
                        string postURL = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SoftCopyUploadURL"]);
                        postURL = postURL + "\"leadId\":" + BookID + " , \"customerId\" :" + CustID + " , \"productId\" :" + ProdID + " }";
                        dynamic DataResult = GetPostAPIResponse(postURL, ms, CustomerBooking.FileName);

                        if (!String.IsNullOrEmpty(Convert.ToString(DataResult.policyCopyDetails.policyDocUrl.Value)))
                        {
                            CustomerBooking.DocumentURL = Convert.ToString(DataResult.policyCopyDetails.policyDocUrl.Value);
                        }
                    }
                }
                Database db = DatabaseFactory.CreateDatabase("ConnectionStringNew");
                DbCommand dbCommand = db.GetStoredProcCommand("[customer].[UpdateDocumentDetails]");
                db.AddInParameter(dbCommand, "@CustomerID", DbType.Int64, CustomerBooking.CustomerID);
                db.AddInParameter(dbCommand, "@LeadID", DbType.Int64, CustomerBooking.LeadID);
                db.AddInParameter(dbCommand, "@DocCategoryID", DbType.Int16, CustomerBooking.DocCategoryID);
                db.AddInParameter(dbCommand, "@DocumentID", DbType.Int16, CustomerBooking.DocumentID);
                db.AddInParameter(dbCommand, "@DocumentUrl", DbType.String, CustomerBooking.DocumentURL);
                if (CustomerBooking.ParentCustDocumentID != 0)
                    db.AddInParameter(dbCommand, "@ParentCustDocumentID", DbType.Int32, CustomerBooking.ParentCustDocumentID);
                db.AddInParameter(dbCommand, "@HostName", DbType.String, CustomerBooking.HostName);
                db.AddInParameter(dbCommand, "@Mode", DbType.Int16, CustomerBooking.Flag);
                db.AddInParameter(dbCommand, "@UserId", DbType.Int16, CustomerBooking.UserID);
                db.ExecuteNonQuery(dbCommand);
                Message = "Details has been saved successfully.";

                return Message;
            }
            catch (Exception ex) { Exc = ex.Message.ToString(); return ex.Message.ToString(); }
            finally
            {
                LoggingHelper.LoggingHelper.LogIntoFile(JsonConvert.SerializeObject(CustomerBooking), JsonConvert.SerializeObject(Message), Exc, "UpdateDocumentDetails");
            }
        }

        public DocumentOptionByCategoryDetails GetDocumentDetails(long LeadID, long CustomerID, short Type)
        {
            string Message = string.Empty;
            string Exc = string.Empty;
            DocumentCategoryDetails dCat = null;
            DocumentOptionByCategoryDetails dobCat = null;
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionStringNew");
                DbCommand dbCommand = db.GetStoredProcCommand("[customer].[GetDocumentDetails]");
                db.AddInParameter(dbCommand, "@CustomerID", DbType.Int64, CustomerID);
                db.AddInParameter(dbCommand, "@LeadID", DbType.Int64, LeadID);
                db.AddInParameter(dbCommand, "@Type", DbType.Int16, Type);
                DataSet ds = db.ExecuteDataSet(dbCommand);

                if (ds.Tables.Count > 0)
                {
                    dobCat = new DocumentOptionByCategoryDetails();
                    dobCat.LeadID = LeadID;
                    dobCat.CustomerID = CustomerID;
                    DataRow dr = ds.Tables[0].Rows[0];
                    dobCat.ProductId = Convert.ToInt32(dr["ProductId"]);
                    dobCat.ProductName = Convert.ToString(dr["ProductName"]);
                    dobCat.SupplierId = Convert.ToInt32(dr["SupplierId"]);
                    dobCat.SupplierName = Convert.ToString(dr["SupplierName"]);
                    dobCat.PlanId = Convert.ToInt32(dr["PlanId"]);
                    dobCat.PlanName = Convert.ToString(dr["PlanName"]);

                    List<DocumentDetails> _objListMenu = new List<DocumentDetails>();
                    var data1 = ds.Tables[1].AsEnumerable();
                    var data2 = ds.Tables[2].AsEnumerable();
                    var data3 = ds.Tables[3].AsEnumerable();
                    _objListMenu = (from users in data1
                                    select new DocumentDetails()
                                        {
                                            DocCategoryID = Convert.ToInt16(users["docCategoryId"]),
                                            DocCategoryName = Convert.ToString(users["docCategory"]),
                                            DocumentID = Convert.ToInt16(users["docTypeId"]),
                                            Document = Convert.ToString(users["docType"])
                                        }).ToList();


                    var b = _objListMenu.GroupBy(test => test.DocCategoryName)
                           .Select(grp => grp)
                           .ToList();
                    var c = _objListMenu.GroupBy(test => test.DocCategoryID)
                           .Select(grp => grp)
                           .ToList();
                    
                    List<DocumentCategoryDetails> docOptionsByCategory = new List<DocumentCategoryDetails>();
                    for (int i = 0; i < c.Count; i++)
                    {
                        dCat = new DocumentCategoryDetails();
                        dCat.DocCategoryID = c.ElementAt(i).Key;
                        dCat.DocCategoryName = b.ElementAt(i).Key;
                        List<DocumentURLDetails> DocURL1s = new List<DocumentURLDetails>();
                        DocumentURLDetails docURL1 = null;
                        foreach (var item2 in data3)
                        {
                            if (Convert.ToInt16(item2["DocCategoryID"]) == c.ElementAt(i).Key)
                            {
                                docURL1 = new DocumentURLDetails();
                                docURL1.CustDocumentID = Convert.ToInt32(item2["CustDocumentID"]);
                                docURL1.DocCategoryID = Convert.ToInt16(item2["DocCategoryID"]);
                                docURL1.Document = Convert.ToString(item2["Document"]);
                                docURL1.DocumentUrl = Convert.ToString(item2["DocumentUrl"]);
                                docURL1.HostName = Convert.ToString(item2["HostName"]);
                                DocURL1s.Add(docURL1);
                            }
                        }
                        dCat.LeadDocument = DocURL1s;
                        List<DocumentURLDetails> DocURLs = new List<DocumentURLDetails>();
                        DocumentURLDetails docURL = null;
                        foreach (var item1 in data2)
                        {
                            if (Convert.ToInt16(item1["DocCategoryID"]) == c.ElementAt(i).Key)
                            {
                                int k = 0;
                                foreach (var item in DocURL1s)
                                {
                                    if (item.DocumentUrl == Convert.ToString(item1["DocumentUrl"]) && item.DocCategoryID == c.ElementAt(i).Key)
                                        k = 1;
                                }
                                if (k == 0)
                                {
                                    docURL = new DocumentURLDetails();
                                    docURL.CustDocumentID = Convert.ToInt32(item1["CustDocumentID"]);
                                    docURL.Document = Convert.ToString(item1["Document"]);
                                    docURL.DocumentUrl = Convert.ToString(item1["DocumentUrl"]);
                                    docURL.HostName = Convert.ToString(item1["HostName"]);
                                    DocURLs.Add(docURL);
                                }
                            }
                        }
                        dCat.CustomerDocument = DocURLs;

                        List<DocumentDetails> dDetail = new List<DocumentDetails>();
                        foreach (DocumentDetails item in _objListMenu)
                        {
                            if(item.DocCategoryID == c.ElementAt(i).Key)
                            {
                                dDetail.Add(item);
                            }
                        } 
                        dCat.MasterDocument =dDetail;
                        docOptionsByCategory.Add(dCat);
                    }

                    dobCat.docOptionsByCategory = docOptionsByCategory;

                }

                return dobCat;
            }
            catch (Exception ex) { Exc = ex.Message.ToString(); return dobCat; }
            finally
            {
                LoggingHelper.LoggingHelper.LogIntoFile(JsonConvert.SerializeObject(LeadID), JsonConvert.SerializeObject(LeadID), Exc, "GetDocumentDetails");
            }
        }

        public string InsertSoftCopyDetails(long LeadId, string SoftCopyURL, int UserID, short Flag)
        {

            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("[BMS].[InsertSoftCopyDetails]");
            db.AddInParameter(dbCommand, "@LeadID", DbType.Int64, LeadId);
            db.AddInParameter(dbCommand, "@SoftCopyURL", DbType.String, SoftCopyURL);
            db.AddInParameter(dbCommand, "@UserID", DbType.Int32, UserID);
            db.AddInParameter(dbCommand, "@Mode", DbType.Int16, Flag);
            string returnValue = Convert.ToString(db.ExecuteNonQuery(dbCommand));
            dbCommand.Dispose();
            return returnValue;
        }

        dynamic GetPostAPIResponse(string URL, Stream file, string FileName)
        {
            dynamic DataResult = null;
            string extention = Path.GetExtension(FileName);
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
            //dynamic PayloadData = "{\"file \" :" + data + ", \"remarks \" :" + FileName + " }"; ;
            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Uri.EscapeUriString(URL));
            request.Method = "POST";
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            request.KeepAlive = true;
            var postData = request.GetRequestStream();
            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, "file", FileName, "application/" + extention);

            postData.Write(boundarybytes, 0, boundarybytes.Length);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            postData.Write(headerbytes, 0, headerbytes.Length);


            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = file.Read(buffer, 0, buffer.Length)) != 0)
            {
                postData.Write(buffer, 0, bytesRead);
            }

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            postData.Write(trailer, 0, trailer.Length);


            postData.Close();

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            string strREsp = responseReader.ReadToEnd();
                            DataResult = JsonConvert.DeserializeObject<dynamic>(strREsp);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
            }


            return DataResult;
        }

        public string UploadFileDetails(UploadDocumentData _uploadDocumentData)
        {
            byte[] data = System.Convert.FromBase64String(_uploadDocumentData.FileStream);
            MemoryStream ms = new MemoryStream(data);
            string Message = string.Empty;
            string Exc = string.Empty;
            try
            {
                string extPath = Path.GetExtension(_uploadDocumentData.FileName).ToLower();
                if (_uploadDocumentData.UploadMode == "FTP")
                {
                    StringBuilder MasterFolderPath = new StringBuilder(System.Configuration.ConfigurationManager.AppSettings["CustomerSpecificDocPath"].ToString());
                    MasterFolderPath.Replace("@CustomerID", Convert.ToString(_uploadDocumentData.CustomerID));
                    MasterFolderPath.Replace("@LeadId", Convert.ToString(_uploadDocumentData.LeadID));
                    MasterFolderPath.Replace("@Year", Convert.ToString(DateTime.Now.Year));
                    MasterFolderPath.Replace("@ProductName", Convert.ToString(_uploadDocumentData.ProductName));
                    if (ServerFileUpload.UploadFileToFTP(ms, Convert.ToString(MasterFolderPath), _uploadDocumentData.FileName))
                    {
                        Message = InsertSoftCopyDetails(Convert.ToInt32(_uploadDocumentData.LeadID), MasterFolderPath + "/" + _uploadDocumentData.FileName, Convert.ToInt32(_uploadDocumentData.UserID), _uploadDocumentData.Flag);
                        Message = MasterFolderPath + "/" + _uploadDocumentData.FileName;
                    }
                }
                else if (_uploadDocumentData.UploadMode == "Mongo")
                {
                    var CustID = Convert.ToString(_uploadDocumentData.CustomerID);
                    var ProdID = Convert.ToString(_uploadDocumentData.ProductID);
                    var BookID = Convert.ToString(_uploadDocumentData.LeadID);
                    string postURL = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SoftCopyUploadURL"]);
                    postURL = postURL + "\"leadId\":" + BookID + " , \"customerId\" :" + CustID + " , \"productId\" :" + ProdID + " }";
                    dynamic DataResult = GetPostAPIResponse(postURL, ms, _uploadDocumentData.FileName);

                    if (!String.IsNullOrEmpty(Convert.ToString(DataResult.policyCopyDetails.policyDocUrl.Value)))
                    {
                        Message = InsertSoftCopyDetails(Convert.ToInt32(_uploadDocumentData.LeadID), Convert.ToString(DataResult.policyCopyDetails.policyDocUrl.Value), Convert.ToInt32(_uploadDocumentData.UserID), _uploadDocumentData.Flag);
                        Message = "Document Uploaded Successfully";
                    }
                }
            }
            catch (Exception ex) { Exc = ex.Message.ToString(); return ex.Message.ToString(); }
            finally
            {
                LoggingHelper.LoggingHelper.LogIntoFile(JsonConvert.SerializeObject(_uploadDocumentData), JsonConvert.SerializeObject(Message), Exc, "UploadDocumentData");
            }
            return Message;
        }

        public string UpdateBooking_ITZCASH(long ItemId, string OrderNo, string Status)
        {

            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("[BMS].[InsertUpdate_BookingStatus_ITZCash]");
            db.AddInParameter(dbCommand, "@ItemId", DbType.Int64, ItemId);
            db.AddInParameter(dbCommand, "@OrderNo", DbType.String, OrderNo);
            db.AddInParameter(dbCommand, "@Status", DbType.String, Status);
            
            DataSet ds = db.ExecuteDataSet(dbCommand);
            DataRow dr = null;
            if (ds.Tables.Count > 1)
            {
                dr = ds.Tables[1].Rows[0];
            }
            else
            {
                dr = ds.Tables[0].Rows[0];
            }
            string returnValue = Convert.ToString(dr["Message"]);
            return returnValue;
        }

        public CustomerSIDetails GetCustomerSIDetails(string ReferenceNo, string MobileNo)
        {

            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("[BMS].[GetCustomerSIDetails]");
            db.AddInParameter(dbCommand, "@ReferenceNo", DbType.String, ReferenceNo);
            db.AddInParameter(dbCommand, "@MobileNo", DbType.String, MobileNo);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            CustomerSIDetails objCustomerSIDetails = new CustomerSIDetails();
            if (ds.Tables.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                objCustomerSIDetails.CustomerName = Convert.ToString(dr["CustomerName"]);
                objCustomerSIDetails.EmailId = Convert.ToString(dr["EmailId"]);
                objCustomerSIDetails.PlanName = Convert.ToString(dr["PlanName"]);
                objCustomerSIDetails.PolicyIssuanceStatus = Convert.ToString(dr["PolicyIssuanceStatus"]);
                objCustomerSIDetails.PolicyIssuanceDate = Convert.ToString(dr["PolicyIssuanceDate"]);
                objCustomerSIDetails.PolicyTerm = Convert.ToString(dr["PolicyTerm"]);
                objCustomerSIDetails.PaymentPeriodicity = Convert.ToString(dr["PaymentPeriodicity"]);
                objCustomerSIDetails.InstallmentsPaid = Convert.ToString(dr["InstallmentsPaid"]);
            }
            dbCommand.Dispose();
            return objCustomerSIDetails;
        }

        public string InsertPCDDetails(PCDData PCDData)
        {
            string Message = string.Empty;
            string Exc = string.Empty;
            Database db = null;
            DbCommand dbCommand = null;
            Message = "Details has been saved successfully.";
            try
            {
                if (PCDData.PolicyDetails != null)
                {
                    db = DatabaseFactory.CreateDatabase("ConnectionStringNew");
                    dbCommand = db.GetStoredProcCommand("[customer].[Insert_Update_PolicyDetails]");
                    db.AddInParameter(dbCommand, "@BookingID", DbType.Int64, PCDData.leadId);
                    db.AddInParameter(dbCommand, "@CustPolicyID", DbType.Int32, PCDData.PolicyDetails.CustPolicyID);
                    db.AddInParameter(dbCommand, "@SupplierId", DbType.Int16, PCDData.PolicyDetails.SupplierId);
                    db.AddInParameter(dbCommand, "@PlanId", DbType.Int16, PCDData.PolicyDetails.PlanId);
                    db.AddInParameter(dbCommand, "@SumInsured", DbType.Decimal, PCDData.PolicyDetails.SumInsured);
                    db.AddInParameter(dbCommand, "@Premium", DbType.Decimal, PCDData.PolicyDetails.Premium);
                    db.AddInParameter(dbCommand, "@PolicyTerm", DbType.Int16, PCDData.PolicyDetails.PolicyTerm);
                    db.AddInParameter(dbCommand, "@PremiumPayingTerm", DbType.Int16, PCDData.PolicyDetails.PremiumPayingTerm);
                    db.AddInParameter(dbCommand, "@PaymentFrequency", DbType.Int16, PCDData.PolicyDetails.PaymentFrequency);
                    db.AddInParameter(dbCommand, "@PolicyNumber", DbType.String, PCDData.PolicyDetails.PolicyNumber);
                    db.AddInParameter(dbCommand, "@GracePeriod", DbType.Int16, PCDData.PolicyDetails.GracePeriod);
                    db.AddInParameter(dbCommand, "@PolicyStartDate", DbType.DateTime, PCDData.PolicyDetails.PolicyStartDate);
                    db.AddInParameter(dbCommand, "@PolicyEndDate", DbType.DateTime, PCDData.PolicyDetails.PolicyEndDate);
                    db.AddInParameter(dbCommand, "@TPA", DbType.Int16, PCDData.PolicyDetails.TPA);
                    db.AddInParameter(dbCommand, "@PreviousPolicyNumber", DbType.String, PCDData.PolicyDetails.PreviousPolicyNumber);
                    db.AddInParameter(dbCommand, "@MinimumDeathBenefit", DbType.Decimal, PCDData.PolicyDetails.MinimumDeathBenefit);
                    db.AddInParameter(dbCommand, "@MaturityBenefit", DbType.Decimal, PCDData.PolicyDetails.MaturityBenefit);
                    db.AddInParameter(dbCommand, "@CustomerId", DbType.Int32, PCDData.PolicyDetails.CustomerId);
                    db.AddInParameter(dbCommand, "@BookingRefID", DbType.Int32, PCDData.PolicyDetails.BookingRefID);
                    db.AddInParameter(dbCommand, "@DocumentURL", DbType.String, PCDData.PolicyDetails.DocumentURL);
                    db.AddInParameter(dbCommand, "@ProposalNumber", DbType.String, PCDData.PolicyDetails.ProposalNumber);
                    db.AddInParameter(dbCommand, "@ProductID", DbType.Int16, PCDData.PolicyDetails.ProductID);
                    db.AddInParameter(dbCommand, "@IsAddOn", DbType.Boolean, PCDData.PolicyDetails.IsAddOn);
                    db.AddInParameter(dbCommand, "@ParentPolicyID", DbType.Int32, PCDData.PolicyDetails.ParentPolicyID);
                    db.AddInParameter(dbCommand, "@CreatedOn", DbType.DateTime, PCDData.PolicyDetails.CreatedOn);
                    db.AddInParameter(dbCommand, "@CreatedBy", DbType.Int16, PCDData.PolicyDetails.CreatedBy);
                    db.AddInParameter(dbCommand, "@UpdatedOn", DbType.DateTime, PCDData.PolicyDetails.UpdatedOn);
                    db.AddInParameter(dbCommand, "@UpdatedBy", DbType.Int16, PCDData.PolicyDetails.UpdatedBy);
                    db.AddInParameter(dbCommand, "@CommAddress", DbType.String, PCDData.PolicyDetails.CommAddress);
                    db.AddInParameter(dbCommand, "@IsSTP", DbType.Boolean, PCDData.PolicyDetails.IsSTP);
                    db.AddInParameter(dbCommand, "@CoverType", DbType.String, PCDData.PolicyDetails.CoverType);
                    db.AddInParameter(dbCommand, "@IssuanceDate", DbType.DateTime, PCDData.PolicyDetails.IssuanceDate);
                    db.AddInParameter(dbCommand, "@ISActive", DbType.Boolean, PCDData.PolicyDetails.ISActive);
                    db.AddInParameter(dbCommand, "@CommPinCode", DbType.String, PCDData.PolicyDetails.CommPinCode);
                    db.AddInParameter(dbCommand, "@CommCityID", DbType.Int16, PCDData.PolicyDetails.CommCityID);
                    db.AddInParameter(dbCommand, "@CommStateID", DbType.Int16, PCDData.PolicyDetails.CommStateID);
                    db.AddInParameter(dbCommand, "@PermAddress", DbType.String, PCDData.PolicyDetails.PermAddress);
                    db.AddInParameter(dbCommand, "@PermPinCode", DbType.String, PCDData.PolicyDetails.PermPinCode);
                    db.AddInParameter(dbCommand, "@PermCityID", DbType.Int16, PCDData.PolicyDetails.PermCityID);
                    db.AddInParameter(dbCommand, "@PermStateID", DbType.Int16, PCDData.PolicyDetails.CommStateID);
                    db.AddInParameter(dbCommand, "@LoadingPercentage", DbType.Int16, PCDData.PolicyDetails.LoadingPercentage);
                    db.AddInParameter(dbCommand, "@LoadingPremium", DbType.Decimal, PCDData.PolicyDetails.LoadingPremium);
                    db.AddInParameter(dbCommand, "@PremiumWaivedOff", DbType.Int16, PCDData.PolicyDetails.PremiumWaivedOff);
                    db.AddInParameter(dbCommand, "@CopaymentPercentage", DbType.Int16, PCDData.PolicyDetails.CopaymentPercentage);
                    DataSet ds = db.ExecuteDataSet(dbCommand);
                    db = null;
                    dbCommand = null;
                }

                if (PCDData.VehicleDetails != null)
                {
                    db = DatabaseFactory.CreateDatabase("ConnectionStringNew");
                    dbCommand = db.GetStoredProcCommand("[customer].[Insert_Update_VehicleDetails]");
                    db.AddInParameter(dbCommand, "@BookingID", DbType.Int64, PCDData.leadId);
                    db.AddInParameter(dbCommand, "@CustVehicleID", DbType.Int64, PCDData.VehicleDetails.CustVehicleID);
                    db.AddInParameter(dbCommand, "@ProductID", DbType.Int16, PCDData.VehicleDetails.ProductID);
                    db.AddInParameter(dbCommand, "@VehicleCode", DbType.Int64, PCDData.VehicleDetails.VehicleCode);
                    db.AddInParameter(dbCommand, "@ChassisNo", DbType.String, PCDData.VehicleDetails.ChassisNo);
                    db.AddInParameter(dbCommand, "@EngineNo", DbType.String, PCDData.VehicleDetails.EngineNo);
                    db.AddInParameter(dbCommand, "@RegistrationNo", DbType.String, PCDData.VehicleDetails.RegistrationNo);
                    db.AddInParameter(dbCommand, "@ExpiryDate", DbType.DateTime, PCDData.VehicleDetails.ExpiryDate);
                    db.AddInParameter(dbCommand, "@CurrentInsurerId", DbType.Int16, PCDData.VehicleDetails.CurrentInsurerId);
                    db.AddInParameter(dbCommand, "@ManufacturingDate", DbType.DateTime, PCDData.VehicleDetails.ManufacturingDate);
                    db.AddInParameter(dbCommand, "@CustomerId", DbType.Int32, PCDData.VehicleDetails.CustomerId);
                    db.AddInParameter(dbCommand, "@VariantId", DbType.Int32, PCDData.VehicleDetails.VariantId);
                    db.AddInParameter(dbCommand, "@ModelId", DbType.Int32, PCDData.VehicleDetails.ModelId);
                    db.AddInParameter(dbCommand, "@MakeId", DbType.Int32, PCDData.VehicleDetails.MakeId);
                    db.AddInParameter(dbCommand, "@CustomerAddressId", DbType.Int64, PCDData.VehicleDetails.CustomerAddressId);
                    db.AddInParameter(dbCommand, "@CustPolicyID", DbType.Int32, PCDData.VehicleDetails.CustPolicyID);
                    db.AddInParameter(dbCommand, "@VehilceTypeId", DbType.Int16, PCDData.VehicleDetails.VehilceTypeId);
                    db.AddInParameter(dbCommand, "@RegistrationDate", DbType.DateTime, PCDData.VehicleDetails.RegistrationDate);
                    db.AddInParameter(dbCommand, "@FuelTypeId", DbType.Int16, PCDData.VehicleDetails.FuelTypeId);
                    db.AddInParameter(dbCommand, "@CubicCapacityId", DbType.Int16, PCDData.VehicleDetails.CubicCapacityId);
                    db.AddInParameter(dbCommand, "@SeatingCapacity", DbType.Int16, PCDData.VehicleDetails.SeatingCapacity);
                    db.AddInParameter(dbCommand, "@VehicleIDV", DbType.Decimal, PCDData.VehicleDetails.VehicleIDV);
                    db.AddInParameter(dbCommand, "@ElectricalAccessoriesIDV", DbType.Decimal, PCDData.VehicleDetails.ElectricalAccessoriesIDV);
                    db.AddInParameter(dbCommand, "@NonElectricalAccessoriesIDV", DbType.Decimal, PCDData.VehicleDetails.NonElectricalAccessoriesIDV);
                    db.AddInParameter(dbCommand, "@CNGLPGUnitIDV", DbType.Decimal, PCDData.VehicleDetails.CNGLPGUnitIDV);
                    db.AddInParameter(dbCommand, "@TotalIDV", DbType.Decimal, PCDData.VehicleDetails.TotalIDV);
                    db.AddInParameter(dbCommand, "@CreateDate", DbType.DateTime, PCDData.VehicleDetails.CreateDate);
                    db.AddInParameter(dbCommand, "@CreatedBy", DbType.Int16, PCDData.VehicleDetails.CreatedBy);
                    db.AddInParameter(dbCommand, "@UpdateDate", DbType.DateTime, PCDData.VehicleDetails.UpdateDate);
                    db.AddInParameter(dbCommand, "@UpdatedBy", DbType.Int16, PCDData.VehicleDetails.UpdatedBy);
                    db.AddInParameter(dbCommand, "@RegAddress", DbType.String, PCDData.VehicleDetails.RegAddress);
                    db.AddInParameter(dbCommand, "@RegisteredStateId", DbType.Int16, PCDData.VehicleDetails.RegisteredStateId);
                    db.AddInParameter(dbCommand, "@RegisteredCityId", DbType.Int16, PCDData.VehicleDetails.RegisteredCityId);
                    db.AddInParameter(dbCommand, "@RegistrationCode", DbType.String, PCDData.VehicleDetails.RegistrationCode);
                    db.AddInParameter(dbCommand, "@RegistrationRTOCode", DbType.String, PCDData.VehicleDetails.RegistrationRTOCode);
                    db.AddInParameter(dbCommand, "@RegistrationPostOfficeVORef", DbType.Int32, PCDData.VehicleDetails.RegistrationPostOfficeVORef);
                    db.AddInParameter(dbCommand, "@RegistrationPostCodeLocality", DbType.String, PCDData.VehicleDetails.RegistrationPostCodeLocality);
                    db.AddInParameter(dbCommand, "@PreviousInsurerAddress", DbType.String, PCDData.VehicleDetails.PreviousInsurerAddress);
                    DataSet ds = db.ExecuteDataSet(dbCommand);
                    db = null;
                    dbCommand = null;
                }

                if (PCDData.AddVehicleDetails != null)
                {
                    db = DatabaseFactory.CreateDatabase("ConnectionStringNew");
                    dbCommand = db.GetStoredProcCommand("[customer].[Insert_Update_AdditionalVehicleDetails]");
                    db.AddInParameter(dbCommand, "@BookingID", DbType.Int64, PCDData.leadId);
                    db.AddInParameter(dbCommand, "@CustAddVehicleID", DbType.Int64, PCDData.AddVehicleDetails.CustAddVehicleID);
                    db.AddInParameter(dbCommand, "@CustVehicleID", DbType.Int64, PCDData.AddVehicleDetails.CustVehicleID);
                    db.AddInParameter(dbCommand, "@RegisteredOwnerAddress", DbType.String, PCDData.AddVehicleDetails.RegisteredOwnerAddress);
                    db.AddInParameter(dbCommand, "@RegistrationPostCode", DbType.String, PCDData.AddVehicleDetails.RegistrationPostCode);
                    db.AddInParameter(dbCommand, "@RegistrationContactNo", DbType.String, PCDData.AddVehicleDetails.RegistrationContactNo);
                    db.AddInParameter(dbCommand, "@PreviousInsurerId", DbType.Int16, PCDData.AddVehicleDetails.PreviousInsurerId);
                    db.AddInParameter(dbCommand, "@VehicleOwnedBy", DbType.String, PCDData.AddVehicleDetails.VehicleOwnedBy);
                    db.AddInParameter(dbCommand, "@OrganizationName", DbType.String, PCDData.AddVehicleDetails.OrganizationName);
                    db.AddInParameter(dbCommand, "@ContactPersonInOrganization", DbType.String, PCDData.AddVehicleDetails.ContactPersonInOrganization);
                    db.AddInParameter(dbCommand, "@IsVehicleRegisteredOwner", DbType.Boolean, PCDData.AddVehicleDetails.IsVehicleRegisteredOwner);
                    db.AddInParameter(dbCommand, "@IsVehicleBoughtWithinLast12Months", DbType.Boolean, PCDData.AddVehicleDetails.IsVehicleBoughtWithinLast12Months);
                    db.AddInParameter(dbCommand, "@IsRCEndorsedInCurrentOwnerName", DbType.Boolean, PCDData.AddVehicleDetails.IsRCEndorsedInCurrentOwnerName);
                    db.AddInParameter(dbCommand, "@RCOwnerName", DbType.String, PCDData.AddVehicleDetails.RCOwnerName);
                    db.AddInParameter(dbCommand, "@CarUsedFor", DbType.String, PCDData.AddVehicleDetails.CarUsedFor);
                    db.AddInParameter(dbCommand, "@IsClaimsMadeInPreviousPolicy", DbType.Boolean, PCDData.AddVehicleDetails.IsClaimsMadeInPreviousPolicy);
                    db.AddInParameter(dbCommand, "@NotClaimedSince", DbType.Int16, PCDData.AddVehicleDetails.NotClaimedSince);
                    db.AddInParameter(dbCommand, "@RegisteredOwnerDOB", DbType.DateTime, PCDData.AddVehicleDetails.RegisteredOwnerDOB);
                    db.AddInParameter(dbCommand, "@VoluntaryExcess", DbType.Int16, PCDData.AddVehicleDetails.VoluntaryExcess);
                    db.AddInParameter(dbCommand, "@ProfessionId", DbType.Int16, PCDData.AddVehicleDetails.ProfessionId);
                    db.AddInParameter(dbCommand, "@IsAutomobileAssociationMember", DbType.Boolean, PCDData.AddVehicleDetails.IsAutomobileAssociationMember);
                    db.AddInParameter(dbCommand, "@AutomobileAssociationName", DbType.String, PCDData.AddVehicleDetails.AutomobileAssociationName);
                    db.AddInParameter(dbCommand, "@AutomobileAssociationMembershipNo", DbType.String, PCDData.AddVehicleDetails.AutomobileAssociationMembershipNo);
                    db.AddInParameter(dbCommand, "@AutomobileAssociationMembershipExpiryDate", DbType.DateTime, PCDData.AddVehicleDetails.AutomobileAssociationMembershipExpiryDate);
                    db.AddInParameter(dbCommand, "@IsAntiTheftDeviceApprovedByARAI", DbType.Boolean, PCDData.AddVehicleDetails.IsAntiTheftDeviceApprovedByARAI);
                    db.AddInParameter(dbCommand, "@IsAvailAgeDiscount", DbType.Boolean, PCDData.AddVehicleDetails.IsAvailAgeDiscount);
                    db.AddInParameter(dbCommand, "@LoanType", DbType.String, PCDData.AddVehicleDetails.LoanType);
                    db.AddInParameter(dbCommand, "@FinancialInstitutionName", DbType.String, PCDData.AddVehicleDetails.FinancialInstitutionName);
                    db.AddInParameter(dbCommand, "@FinancialInstitutionCode", DbType.String, PCDData.AddVehicleDetails.FinancialInstitutionCode);
                    db.AddInParameter(dbCommand, "@FinancialInstitutionAddress", DbType.String, PCDData.AddVehicleDetails.FinancialInstitutionAddress);
                    db.AddInParameter(dbCommand, "@FinancialInstitutionCity", DbType.String, PCDData.AddVehicleDetails.FinancialInstitutionCity);
                    db.AddInParameter(dbCommand, "@VehicleDrivenBy", DbType.String, PCDData.AddVehicleDetails.VehicleDrivenBy);
                    db.AddInParameter(dbCommand, "@DriverFullName", DbType.String, PCDData.AddVehicleDetails.DriverFullName);
                    db.AddInParameter(dbCommand, "@DrivingExperience", DbType.Int16, PCDData.AddVehicleDetails.DrivingExperience);
                    db.AddInParameter(dbCommand, "@DriverDateOfBirth", DbType.DateTime, PCDData.AddVehicleDetails.DriverDateOfBirth);
                    db.AddInParameter(dbCommand, "@DriverAge", DbType.Int16, PCDData.AddVehicleDetails.DriverAge);
                    db.AddInParameter(dbCommand, "@TotalDrivers", DbType.Int16, PCDData.AddVehicleDetails.TotalDrivers);
                    db.AddInParameter(dbCommand, "@DriverGender", DbType.String, PCDData.AddVehicleDetails.DriverGender);
                    db.AddInParameter(dbCommand, "@ParkingType", DbType.String, PCDData.AddVehicleDetails.ParkingType);
                    db.AddInParameter(dbCommand, "@DriverMaritalStatus", DbType.String, PCDData.AddVehicleDetails.DriverMaritalStatus);
                    db.AddInParameter(dbCommand, "@AnnualKilometersRuns", DbType.String, PCDData.AddVehicleDetails.AnnualKilometersRuns);
                    db.AddInParameter(dbCommand, "@TotalNumberOfClaimsMade", DbType.Int16, PCDData.AddVehicleDetails.TotalNumberOfClaimsMade);
                    db.AddInParameter(dbCommand, "@NCBDocumentHolds", DbType.String, PCDData.AddVehicleDetails.NCBDocumentHolds);
                    db.AddInParameter(dbCommand, "@LicenseType", DbType.String, PCDData.AddVehicleDetails.LicenseType);
                    db.AddInParameter(dbCommand, "@LicenseAge", DbType.Int16, PCDData.AddVehicleDetails.LicenseAge);
                    db.AddInParameter(dbCommand, "@IsEffectiveDL", DbType.Boolean, PCDData.AddVehicleDetails.IsEffectiveDL);
                    db.AddInParameter(dbCommand, "@IsUnderLoan", DbType.Boolean, PCDData.AddVehicleDetails.IsUnderLoan);
                    db.AddInParameter(dbCommand, "@VehicleMostlyUsed", DbType.String, PCDData.AddVehicleDetails.VehicleMostlyUsed);
                    db.AddInParameter(dbCommand, "@LicenseNumber", DbType.String, PCDData.AddVehicleDetails.LicenseNumber);
                    db.AddInParameter(dbCommand, "@LicenseIssueDate", DbType.DateTime, PCDData.AddVehicleDetails.LicenseIssueDate);
                    db.AddInParameter(dbCommand, "@LicenseExpDate", DbType.DateTime, PCDData.AddVehicleDetails.LicenseExpDate);
                    db.AddInParameter(dbCommand, "@LicenseIssuingAuthority", DbType.String, PCDData.AddVehicleDetails.LicenseIssuingAuthority);
                    db.AddInParameter(dbCommand, "@VehicleColor", DbType.String, PCDData.AddVehicleDetails.VehicleColor);
                    db.AddInParameter(dbCommand, "@IsElectricalAccessories", DbType.Boolean, PCDData.AddVehicleDetails.IsElectricalAccessories);
                    db.AddInParameter(dbCommand, "@ElectricalAccessoriesInvoiceDate", DbType.DateTime, PCDData.AddVehicleDetails.ElectricalAccessoriesInvoiceDate);
                    db.AddInParameter(dbCommand, "@IsNonElectricalAccessories", DbType.Boolean, PCDData.AddVehicleDetails.IsNonElectricalAccessories);
                    db.AddInParameter(dbCommand, "@NonElectricalAccessoriesInvoiceDate", DbType.DateTime, PCDData.AddVehicleDetails.NonElectricalAccessoriesInvoiceDate);
                    db.AddInParameter(dbCommand, "@IsCNGFitted", DbType.Boolean, PCDData.AddVehicleDetails.IsCNGFitted);
                    db.AddInParameter(dbCommand, "@TypeOfCNGKit", DbType.String, PCDData.AddVehicleDetails.TypeOfCNGKit);
                    db.AddInParameter(dbCommand, "@CNGInvoiceDate", DbType.DateTime, PCDData.AddVehicleDetails.CNGInvoiceDate);
                    db.AddInParameter(dbCommand, "@IsPACoverForUnnamedPassengers", DbType.Boolean, PCDData.AddVehicleDetails.IsPACoverForUnnamedPassengers);
                    db.AddInParameter(dbCommand, "@PACoverForUnnamedPassengers", DbType.Decimal, PCDData.AddVehicleDetails.PACoverForUnnamedPassengers);
                    db.AddInParameter(dbCommand, "@IsInsurePaidDriver", DbType.Boolean, PCDData.AddVehicleDetails.IsInsurePaidDriver);
                    db.AddInParameter(dbCommand, "@PaidDriverLiabilityAmount", DbType.Decimal, PCDData.AddVehicleDetails.PaidDriverLiabilityAmount);
                    db.AddInParameter(dbCommand, "@CreateDate", DbType.DateTime, PCDData.AddVehicleDetails.CreateDate);
                    db.AddInParameter(dbCommand, "@CreatedBy", DbType.Int16, PCDData.AddVehicleDetails.CreatedBy);
                    db.AddInParameter(dbCommand, "@UpdateDate", DbType.DateTime, PCDData.AddVehicleDetails.UpdateDate);
                    db.AddInParameter(dbCommand, "@UpdatedBy", DbType.Int16, PCDData.AddVehicleDetails.UpdatedBy);
                    db.AddInParameter(dbCommand, "@ClaimAmount", DbType.Int32, PCDData.AddVehicleDetails.ClaimAmount);
                    db.AddInParameter(dbCommand, "@IsPrePolicyHaveZeroDep", DbType.Boolean, PCDData.AddVehicleDetails.IsPrePolicyHaveZeroDep);
                    db.AddInParameter(dbCommand, "@ValueOfElectricalAccessories", DbType.Decimal, PCDData.AddVehicleDetails.ValueOfElectricalAccessories);
                    db.AddInParameter(dbCommand, "@ValueOfNonElectricalAccessories", DbType.Decimal, PCDData.AddVehicleDetails.ValueOfNonElectricalAccessories);
                    db.AddInParameter(dbCommand, "@CNGAmount", DbType.Decimal, PCDData.AddVehicleDetails.CNGAmount);
                    db.AddInParameter(dbCommand, "@Title", DbType.String, PCDData.AddVehicleDetails.Title);
                    db.AddInParameter(dbCommand, "@PACoverForOwnerDriver", DbType.String, PCDData.AddVehicleDetails.PACoverForOwnerDriver);
                    DataSet ds = db.ExecuteDataSet(dbCommand);
                    db = null;
                    dbCommand = null;
                }
                if (PCDData.AdditionalCoverItemVehicleDetails != null)
                {
                    if (PCDData.AdditionalCoverItemVehicleDetails.Length > 0)
                    {
                        for (int k = 0; k < PCDData.AdditionalCoverItemVehicleDetails.Length; k++)
                        {
                            db = DatabaseFactory.CreateDatabase("ConnectionStringNew");
                            dbCommand = db.GetStoredProcCommand("[customer].[Insert_Update_AdditionalCoverItemVehicleDetails]");
                            db.AddInParameter(dbCommand, "@BookingID", DbType.Int64, PCDData.leadId);
                            db.AddInParameter(dbCommand, "@CustAddCoverVehicleID", DbType.Int64, PCDData.AdditionalCoverItemVehicleDetails[k].CustAddCoverVehicleID);
                            db.AddInParameter(dbCommand, "@CustVehicleID", DbType.Int64, PCDData.AdditionalCoverItemVehicleDetails[k].CustVehicleID);
                            db.AddInParameter(dbCommand, "@ItemType", DbType.Int16, PCDData.AdditionalCoverItemVehicleDetails[k].ItemType);
                            db.AddInParameter(dbCommand, "@ItemName", DbType.String, PCDData.AdditionalCoverItemVehicleDetails[k].ItemName);
                            db.AddInParameter(dbCommand, "@ItemMakeModel", DbType.String, PCDData.AdditionalCoverItemVehicleDetails[k].ItemMakeModel);
                            db.AddInParameter(dbCommand, "@ItemMfgYear", DbType.Int16, PCDData.AdditionalCoverItemVehicleDetails[k].ItemMfgYear);
                            db.AddInParameter(dbCommand, "@ItemAmount", DbType.Decimal, PCDData.AdditionalCoverItemVehicleDetails[k].ItemAmount);
                            db.AddInParameter(dbCommand, "@CreateDate", DbType.DateTime, PCDData.AdditionalCoverItemVehicleDetails[k].CreateDate);
                            db.AddInParameter(dbCommand, "@CreatedBy", DbType.Int16, PCDData.AdditionalCoverItemVehicleDetails[k].CreatedBy);
                            db.AddInParameter(dbCommand, "@UpdateDate", DbType.DateTime, PCDData.AdditionalCoverItemVehicleDetails[k].UpdateDate);
                            db.AddInParameter(dbCommand, "@UpdatedBy", DbType.Int16, PCDData.AdditionalCoverItemVehicleDetails[k].UpdatedBy);
                            DataSet ds = db.ExecuteDataSet(dbCommand);
                            db = null;
                            dbCommand = null;
                        }
                    }
                }
                if (PCDData.NomineeDetails != null)
                {
                    if (PCDData.NomineeDetails.Length > 0)
                    {
                        for (int k = 0; k < PCDData.NomineeDetails.Length; k++)
                        {
                            db = DatabaseFactory.CreateDatabase("ConnectionStringNew");
                            dbCommand = db.GetStoredProcCommand("[customer].[Insert_Update_PolicyNomineeDetails]");
                            db.AddInParameter(dbCommand, "@BookingID", DbType.Int64, PCDData.leadId);
                            db.AddInParameter(dbCommand, "@PolicyNomineeID", DbType.Int32, PCDData.NomineeDetails[k].PolicyNomineeID);
                            db.AddInParameter(dbCommand, "@CustPolicyID", DbType.Int32, PCDData.NomineeDetails[k].CustPolicyID);
                            db.AddInParameter(dbCommand, "@NomineeMemberID", DbType.Int32, PCDData.NomineeDetails[k].NomineeMemberID);
                            db.AddInParameter(dbCommand, "@AppointeeMemberID", DbType.Int32, PCDData.NomineeDetails[k].AppointeeMemberID);
                            db.AddInParameter(dbCommand, "@PercentageShare", DbType.Int16, PCDData.NomineeDetails[k].PercentageShare);
                            db.AddInParameter(dbCommand, "@CreatedOn", DbType.DateTime, PCDData.NomineeDetails[k].CreatedOn);
                            db.AddInParameter(dbCommand, "@CreatedBy", DbType.Int16, PCDData.NomineeDetails[k].CreatedBy);
                            db.AddInParameter(dbCommand, "@UpdatedOn", DbType.DateTime, PCDData.NomineeDetails[k].UpdatedOn);
                            db.AddInParameter(dbCommand, "@UpdatedBy", DbType.Int16, PCDData.NomineeDetails[k].UpdatedBy);
                            db.AddInParameter(dbCommand, "@RelationshipTypeID", DbType.Int16, PCDData.NomineeDetails[k].RelationshipTypeID);
                            db.AddInParameter(dbCommand, "@NomineeAge", DbType.Int16, PCDData.NomineeDetails[k].NomineeAge);
                            db.AddInParameter(dbCommand, "@NomineeRelationship", DbType.String, PCDData.NomineeDetails[k].NomineeRelationship);
                            db.AddInParameter(dbCommand, "@AppointeeAge", DbType.Int16, PCDData.NomineeDetails[k].AppointeeAge);
                            db.AddInParameter(dbCommand, "@AppointeeRelationship", DbType.String, PCDData.NomineeDetails[k].AppointeeRelationship);
                            db.AddInParameter(dbCommand, "@NomineeName", DbType.String, PCDData.NomineeDetails[k].NomineeName);
                            db.AddInParameter(dbCommand, "@AppointeeName", DbType.String, PCDData.NomineeDetails[k].AppointeeName);
                            db.AddInParameter(dbCommand, "@NomineeGender", DbType.String, PCDData.NomineeDetails[k].NomineeGender);
                            DataSet ds = db.ExecuteDataSet(dbCommand);
                            db = null;
                            dbCommand = null;
                        }
                    }
                }

                if (PCDData.InsuredDetails != null)
                {
                    if (PCDData.InsuredDetails.Length > 0)
                    {
                        for (int k = 0; k < PCDData.InsuredDetails.Length; k++)
                        {
                            db = DatabaseFactory.CreateDatabase("ConnectionStringNew");
                            dbCommand = db.GetStoredProcCommand("[customer].[Insert_Update_PolicyInsuredDetails]");
                            db.AddInParameter(dbCommand, "@BookingID", DbType.Int64, PCDData.leadId);
                            db.AddInParameter(dbCommand, "@PolicyInsuredID", DbType.Int32, PCDData.InsuredDetails[k].PolicyInsuredID);
                            db.AddInParameter(dbCommand, "@CustPolicyID", DbType.Int32, PCDData.InsuredDetails[k].CustPolicyID);
                            db.AddInParameter(dbCommand, "@IsPrimary", DbType.Boolean, PCDData.InsuredDetails[k].IsPrimary);
                            db.AddInParameter(dbCommand, "@CreatedOn", DbType.DateTime, PCDData.InsuredDetails[k].CreatedOn);
                            db.AddInParameter(dbCommand, "@CreatedBy", DbType.Int16, PCDData.InsuredDetails[k].CreatedBy);
                            db.AddInParameter(dbCommand, "@UpdatedOn", DbType.DateTime, PCDData.InsuredDetails[k].UpdatedOn);
                            db.AddInParameter(dbCommand, "@UpdatedBy", DbType.Int16, PCDData.InsuredDetails[k].UpdatedBy);
                            db.AddInParameter(dbCommand, "@IsActive", DbType.Boolean, PCDData.InsuredDetails[k].IsActive);
                            db.AddInParameter(dbCommand, "@InsuredName", DbType.String, PCDData.InsuredDetails[k].InsuredName);
                            db.AddInParameter(dbCommand, "@DOB", DbType.Date, PCDData.InsuredDetails[k].DOB);
                            db.AddInParameter(dbCommand, "@Gender", DbType.Int16, PCDData.InsuredDetails[k].Gender);
                            db.AddInParameter(dbCommand, "@IncomeGroupID", DbType.Int16, PCDData.InsuredDetails[k].IncomeGroupID);
                            db.AddInParameter(dbCommand, "@MaritalStatusID", DbType.Int16, PCDData.InsuredDetails[k].MaritalStatusID);
                            db.AddInParameter(dbCommand, "@RelationWithProposerID", DbType.Int16, PCDData.InsuredDetails[k].RelationWithProposerID);
                            db.AddInParameter(dbCommand, "@Title", DbType.String, PCDData.InsuredDetails[k].Title);
                            db.AddInParameter(dbCommand, "@OccupationId", DbType.Int16, PCDData.InsuredDetails[k].OccupationId);
                            db.AddInParameter(dbCommand, "@Email", DbType.String, PCDData.InsuredDetails[k].Email);
                            db.AddInParameter(dbCommand, "@MobileNo", DbType.Int64, Convert.ToInt64(PCDData.InsuredDetails[k].MobileNo));
                            DataSet ds = db.ExecuteDataSet(dbCommand);
                            db = null;
                            dbCommand = null;
                        }
                    }
                }
                if (PCDData.AddInsuredDetails != null)
                {
                    if (PCDData.AddInsuredDetails.Length > 0)
                    {
                        for (int k = 0; k < PCDData.AddInsuredDetails.Length; k++)
                        {
                            db = DatabaseFactory.CreateDatabase("ConnectionStringNew");
                            dbCommand = db.GetStoredProcCommand("[customer].[Insert_Update_PolicyInsuredAdditionalInfo]");
                            db.AddInParameter(dbCommand, "@BookingID", DbType.Int64, PCDData.leadId);
                            db.AddInParameter(dbCommand, "@InsAddInfoID", DbType.Int32, PCDData.AddInsuredDetails[k].InsAddInfoID);
                            db.AddInParameter(dbCommand, "@PolicyInsuredID", DbType.Int32, PCDData.AddInsuredDetails[k].PolicyInsuredID);
                            db.AddInParameter(dbCommand, "@PANNum", DbType.String, PCDData.AddInsuredDetails[k].PANNum);
                            db.AddInParameter(dbCommand, "@DLNum", DbType.String, PCDData.AddInsuredDetails[k].DLNum);
                            db.AddInParameter(dbCommand, "@Passport", DbType.String, PCDData.AddInsuredDetails[k].Passport);
                            db.AddInParameter(dbCommand, "@Height", DbType.Int16, PCDData.AddInsuredDetails[k].Height);
                            db.AddInParameter(dbCommand, "@Weight", DbType.Int16, PCDData.AddInsuredDetails[k].Weight);
                            db.AddInParameter(dbCommand, "@ChewsTobaccoORSmokes", DbType.Boolean, PCDData.AddInsuredDetails[k].ChewsTobaccoORSmokes);
                            db.AddInParameter(dbCommand, "@AnyPreExistingDisease", DbType.String, PCDData.AddInsuredDetails[k].AnyPreExistingDisease);
                            db.AddInParameter(dbCommand, "@AlcoholORDrugAbuse", DbType.Boolean, PCDData.AddInsuredDetails[k].AlcoholORDrugAbuse);
                            db.AddInParameter(dbCommand, "@AnyHositalizationHistory", DbType.String, PCDData.AddInsuredDetails[k].AnyHositalizationHistory);
                            db.AddInParameter(dbCommand, "@PostOfficeVORef", DbType.Int32, PCDData.AddInsuredDetails[k].PostOfficeVORef);
                            db.AddInParameter(dbCommand, "@PostCodeLocality", DbType.String, PCDData.AddInsuredDetails[k].PostCodeLocality);
                            db.AddInParameter(dbCommand, "@IsRegAddSameAsCommAddress", DbType.Boolean, PCDData.AddInsuredDetails[k].IsRegAddSameAsCommAddress);
                            db.AddInParameter(dbCommand, "@OccupationID", DbType.Int16, PCDData.AddInsuredDetails[k].OccupationID);
                            db.AddInParameter(dbCommand, "@CreatedOn", DbType.DateTime, PCDData.AddInsuredDetails[k].CreatedOn);
                            db.AddInParameter(dbCommand, "@CreatedBy", DbType.Int16, PCDData.AddInsuredDetails[k].CreatedBy);
                            db.AddInParameter(dbCommand, "@UpdatedOn", DbType.DateTime, PCDData.AddInsuredDetails[k].UpdatedOn);
                            db.AddInParameter(dbCommand, "@UpdatedBy", DbType.Int16, PCDData.AddInsuredDetails[k].UpdatedBy);
                            db.AddInParameter(dbCommand, "@InsuredExclusionReason", DbType.String, PCDData.AddInsuredDetails[k].InsuredExclusionReason);
                            db.AddInParameter(dbCommand, "@DiseaseExcluded", DbType.String, PCDData.AddInsuredDetails[k].DiseaseExcluded);
                            db.AddInParameter(dbCommand, "@DiseaseExcludedType", DbType.Int16, PCDData.AddInsuredDetails[k].DiseaseExcludedType);
                            db.AddInParameter(dbCommand, "@DiseaseExcludedWaitingPeriod", DbType.Int16, PCDData.AddInsuredDetails[k].DiseaseExcludedWaitingPeriod);
                            DataSet ds = db.ExecuteDataSet(dbCommand);
                            db = null;
                            dbCommand = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                db = null;
                dbCommand = null;
                Message = "Details has not been saved successfully.";
            }
            finally
            {
                LoggingHelper.LoggingHelper.LogIntoFile(JsonConvert.SerializeObject(PCDData), JsonConvert.SerializeObject(PCDData), Exc, "InsertPCDDetails");
            }


            return Message;
        }
    }
}
