﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
//BMS-342: Added By Sumit
using System.IO;
using Newtonsoft.Json;
//BMS-342: Added By Sumit

namespace BMSRestServices
{
    [DataContract]
    public class PolicyContract
    {
        [DataMember]
        public string LeadId { get; set; }

        [DataMember]
        public string PolicyNumber { get; set; }

        [DataMember]
        public string ProposalNumber { get; set; }

        [DataMember]
        public bool IsSTP { get; set; }

        [DataMember]
        public string PolicyLink { get; set; }

        [DataMember]
        public string IsLinkPB { get; set; }
    }

    [DataContract]
    public class SetCallBackContract
    {
        [DataMember]
        public Int64 LeadId { get; set; }

        [DataMember]
        public Int32 AgentID { get; set; }

        [DataMember]
        public Int32 @MinutesToAdd { get; set; }
    }

    
    [DataContract]
    public class PolicyStageContract
    {
        [DataMember]
        public string LeadId { get; set; }

        [DataMember]
        public string PolicyNumber { get; set; }

        [DataMember]
        public string ProposalNumber { get; set; }

        [DataMember]
        public string IsSTP { get; set; }

        [DataMember]
        public string PolicyLink { get; set; }

        [DataMember]
        public string IsLinkPB { get; set; }

        [DataMember]
        public short SupplierID { get; set; }

        [DataMember]
        public short ProductID { get; set; }
    }

    [DataContract]
    public class CustomerResponseAction
    {
        [DataMember]
        public Int64 LeadId { get; set; }

        [DataMember]
        public Int32 AgentID { get; set; }

        [DataMember]
        public Int32 ActionType { get; set; }

        [DataMember]
        public string CommType { get; set; }

        [DataMember]
        public string TriggerName { get; set; }
    }


    public class BMSWCFRest : IBMSWCFRest
    {

        public string UpdateBookingStatus(PolicyContract PolicyDetails)
        {
            string Message = string.Empty;
            string Exc = string.Empty;
            try
            {
                string json = JsonConvert.SerializeObject(PolicyDetails);
                BusinessLogic bl = new BusinessLogic();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
                sqlQuery.Append(File.ReadAllText(@"D:\BMSRestfulService\InsertUpdateLeadStatusByAPI.txt"));
                //sqlQuery.Append(File.ReadAllText(@"C:\Users\lalan\Desktop\BMS_QA_Post_July15\BMSRestServices_Health_CallTransfer\BMSRestServices\BMSRestServices\InsertUpdateLeadStatusByAPI.txt"));
                sqlQuery.Replace("@LeadIDValue", Convert.ToString(PolicyDetails.LeadId));
                sqlQuery.Replace("@PolicyNoValue", PolicyDetails.PolicyNumber);
                sqlQuery.Replace("@ProposalNoValue", PolicyDetails.ProposalNumber);
                sqlQuery.Replace("@IsSTPValue", Convert.ToInt16(PolicyDetails.IsSTP).ToString());
                sqlQuery.Replace("@PolicyLinkValue", PolicyDetails.PolicyLink);
                sqlQuery.Replace("@IsLinkPBValue", Convert.ToInt16(PolicyDetails.IsLinkPB).ToString());
                DbCommand dbCommand = db.GetSqlStringCommand(sqlQuery.ToString());
                DataSet ds = db.ExecuteDataSet(dbCommand);

                if (ds.Tables.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    Message = Convert.ToString(dr["Message"]);
                    if (Message == "Details Updated")
                        return Message;
                    if (Message != "Status Not Moved")
                    {
                        bl.SendEmailSms(Convert.ToInt64(PolicyDetails.LeadId), Convert.ToString(dr["MobileNo"]), Convert.ToString(dr["EmailID"]), Convert.ToString(dr["StatusID"]), Convert.ToString(dr["SubStatusID"]), Convert.ToString(dr["BookingType"]), Convert.ToString(dr["SupplierId"]));
                    }
                    else
                    {
                        Message = "Status Not Moved";
                    }
                }
                return Message;
            }

            catch (Exception ex) {Exc=ex.Message.ToString(); return ex.Message.ToString(); }
            finally {
                LoggingHelper.LoggingHelper.LogIntoFile(JsonConvert.SerializeObject(PolicyDetails), JsonConvert.SerializeObject(Message), Exc, "UpdateBookingStatus");
            }
        }

        public string SetCallBackInBMS(SetCallBackContract SetCallBackDetails)
        {
            string Message = string.Empty;
            string Exc = string.Empty;
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("[BMS].[Set_AgentServiceRequestCallBackCallTransfer]");
                db.AddInParameter(dbCommand, "@BookingId", DbType.Int64, SetCallBackDetails.LeadId);
                db.AddInParameter(dbCommand, "@AgentID", DbType.Int32, SetCallBackDetails.AgentID);
                db.AddInParameter(dbCommand, "@MinutesToAdd", DbType.Int32, 15);
                DataSet ds = db.ExecuteDataSet(dbCommand);

                if (ds.Tables.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    Message = Convert.ToString(dr["Message"]);
                }
                else
                {
                    Message = "Call Back Not Set: Error code-0";
                }
                dbCommand.Dispose();
                return Message;
            }
            catch (Exception ex) { Exc = ex.Message.ToString(); return ex.Message.ToString(); }
            finally
            {
                LoggingHelper.LoggingHelper.LogIntoFile(JsonConvert.SerializeObject(SetCallBackDetails), JsonConvert.SerializeObject(Message), Exc, "SetCallBackInBMS");
            }
        }

        public string UpdatePolicyStageStatus(PolicyStageContract PolicyStage)
        {
            string Message = "Status Moved";
            string Exc = string.Empty;
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("[BMS].[InsertBookingPolicyStageDetails]");
                db.AddInParameter(dbCommand, "@LeadID", DbType.Int64, PolicyStage.LeadId);
                db.AddInParameter(dbCommand, "@PolicyNo", DbType.String, PolicyStage.PolicyNumber);
                db.AddInParameter(dbCommand, "@ProposalNo", DbType.String, PolicyStage.ProposalNumber);
                if (PolicyStage.IsSTP == "")
                    db.AddInParameter(dbCommand, "@IsSTP", DbType.Boolean, DBNull.Value);
                else
                    db.AddInParameter(dbCommand, "@IsSTP", DbType.Boolean, Convert.ToBoolean(Convert.ToInt16(PolicyStage.IsSTP)));
                db.AddInParameter(dbCommand, "@PolicyLink", DbType.String, PolicyStage.PolicyLink);
                db.AddInParameter(dbCommand, "@IsLinkPB", DbType.String, PolicyStage.IsLinkPB);
                db.AddInParameter(dbCommand, "@SupplierID", DbType.Int16, PolicyStage.SupplierID);
                db.AddInParameter(dbCommand, "@ProductID", DbType.Int16, PolicyStage.ProductID);
                db.AddInParameter(dbCommand, "@InsertType", DbType.String, "Auto");
                int rowsAffected = db.ExecuteNonQuery(dbCommand);
                if (rowsAffected == 1)
                    Message = "Status Moved";
                return Message;
            }

            catch (Exception ex) { Exc = ex.Message.ToString(); return ex.Message.ToString(); }
            finally
            {
                LoggingHelper.LoggingHelper.LogIntoFile(JsonConvert.SerializeObject(PolicyStage), JsonConvert.SerializeObject(Message), Exc, "UpdatePolicyStageStatus");
            }
        }

        public string CustomerResponseAction(CustomerResponseAction CustomerResponse)
        {
            string Message = string.Empty;
            string Exc = string.Empty;
            try
            {
                int res = 0;
                int AllLead = 0;

                int nCommType = 0;
                if (CustomerResponse.CommType.ToUpper() == "EMAIL")
                    nCommType = 24;
                else if (CustomerResponse.CommType.ToUpper() == "SMS")
                    nCommType = 25;
                else if (CustomerResponse.CommType.ToUpper() == "CALL")
                {
                    nCommType = 1;
                    AllLead = 1;
                }

                if ((CustomerResponse.ActionType == 2 && nCommType != 0) || CustomerResponse.ActionType == 1)
                {

                    
                    if (CustomerResponse.TriggerName != null && CustomerResponse.TriggerName.Contains("Reachable")) { AllLead = 1; }
                    Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                    DbCommand dbCommand = db.GetStoredProcCommand("[BMS].[CustResponseAction]");
                    db.AddInParameter(dbCommand, "@LeadID", DbType.Int64, CustomerResponse.LeadId);
                    db.AddInParameter(dbCommand, "@AgentID", DbType.Int32, CustomerResponse.AgentID);
                    db.AddInParameter(dbCommand, "@ActionType", DbType.Int32, CustomerResponse.ActionType);
                    db.AddInParameter(dbCommand, "@CommType", DbType.Int32, nCommType);
                    db.AddInParameter(dbCommand, "@AllLead", DbType.Int32, AllLead);
                    db.AddParameter(dbCommand, "@Result", DbType.Int32, ParameterDirection.Output, "@Result", DataRowVersion.Default, null);
                    res = db.ExecuteNonQuery(dbCommand);
                    int result = (int)db.GetParameterValue(dbCommand, "@Result");
                    switch (result)
                    {
                        case 1:
                            Message = "Callback set.";
                            break;
                        case 2:
                            Message = "Eventtype changed and CommunicationDetails updated.";
                            break;
                        case 3:
                            Message = "Eventtype changed and CommunicationDetails updated.";
                            break;
                        case 4:
                            Message = "Eventtype changed and CommunicationDetails updated.";
                            break;
                        default:
                            Message = "";
                            break;
                    }
                }

                return Message;
            }
            catch (Exception ex) { Exc = ex.Message.ToString(); return ex.Message.ToString(); }
            finally
            {
                LoggingHelper.LoggingHelper.LogIntoFile(JsonConvert.SerializeObject(CustomerResponse), JsonConvert.SerializeObject(Message), Exc, "CustomerResponseAction");
            }
        }

    }
}
