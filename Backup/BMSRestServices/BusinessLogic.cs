﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Net;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BMSRestServices
{
    public class BusinessLogic
    {
        public void SendEmailSms(long leadId, string MobileNo, string EmailID, string StatusID, string SubStatusID, string BookingType, string SupplierId)
        {
            string TriggerName = "";

            if (StatusID == "13")
            {
                TriggerName = "BookingValidatedE2E";
                SendCommunication(leadId, TriggerName, MobileNo, EmailID, BookingType, SupplierId);
            }
            else if (StatusID == "41")
            {
                TriggerName = "BookingValidatedE2E";
                SendCommunication(leadId, TriggerName, MobileNo, EmailID, BookingType, SupplierId);

                TriggerName = "PolicyIssuedE2E";
                SendCommunication(leadId, TriggerName, MobileNo, EmailID, BookingType, SupplierId);
            }

        }

        public void SendCommunication(long leadId, string TriggerName, string MobileNo, string EmailID, string BookingType, string SupplierId)
        {
            string JsonCommModel = "";
            SendMailViaCommService delLoadDataEmail = new SendMailViaCommService(SendEmailSms_Comm);
            SendMailViaCommService delLoadDataSms = new SendMailViaCommService(SendEmailSms_Comm);
            JsonCommModel = "{\"CommunicationDetails\":{\"LeadID\":" + leadId + ",\"IsBooking\":true,\"ProductID\":2, \"Conversations\":[{\"ToReceipent\":[\"" + MobileNo + "\"],\"TriggerName\":\"" + TriggerName + "\",\"CreatedBy\":\"BMS\",\"UserID\":124,\"AutoTemplate\":true,\"SupplierId\":[" + SupplierId + "],\"LeadStatusID\":0,\"SubStatusID\":0}], \"BookingDetail\":{\"BookingType\":\"" + BookingType + "\"} ,\"CommunicationType\":2}}";
            delLoadDataSms.BeginInvoke(JsonCommModel, this.SendMailViaCommServiceComplete, delLoadDataSms);
            JsonCommModel = "{\"CommunicationDetails\":{\"LeadID\":" + leadId + ",\"IsBooking\":true,\"ProductID\":2, \"Conversations\":[{\"From\":\"communication@policybazaar.com\",\"ToReceipent\":[\"" + EmailID + "\"],\"TriggerName\":\"" + TriggerName + "\",\"CreatedBy\":\"BMS\",\"UserID\":124,\"AutoTemplate\":true,\"SupplierId\":[" + SupplierId + "],\"LeadStatusID\":0,\"SubStatusID\":0}], \"BookingDetail\":{\"BookingType\":\"" + BookingType + "\"} ,\"CommunicationType\":1}}";
            delLoadDataEmail.BeginInvoke(JsonCommModel, this.SendMailViaCommServiceComplete, delLoadDataEmail);
        }

        public static void SendEmailSms_Comm(string JsonCommModel)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(ConfigurationManager.AppSettings["CommServiceURLQA"]);
                request.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["CommServiceTimeOut"]);
                request.Method = "POST";
                request.ContentType = "application/json";

                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                byte[] bytes = encoding.GetBytes(JsonCommModel);

                request.ContentLength = bytes.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    // Send the data.
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            var r = JsonConvert.DeserializeObject(responseReader.ReadToEnd());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        delegate void SendMailViaCommService(string JsonCommModel);
        private void SendMailViaCommServiceComplete(IAsyncResult ar)
        {
            SendMailViaCommService d = (SendMailViaCommService)ar.AsyncState;
            d.EndInvoke(ar);
        }
    }


}