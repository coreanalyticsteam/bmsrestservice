﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace BMSRestServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBMSWCFRest" in both code and config file together.
    [ServiceContract]
    public interface IBMSWCFRest
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/UpdateBookingStatus")]
        string UpdateBookingStatus(PolicyContract PolicyDetails); 
        //string UpdateBookingStatus(long leadId, string PolicyNumber, string ProposalNumber, bool IsSTP, string PolicyLink, bool IsLinkPB);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/SetCallBackInBMS")]
        string SetCallBackInBMS(SetCallBackContract SetCallBackDetails);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/UpdatePolicyStageStatus")]
        string UpdatePolicyStageStatus(PolicyStageContract PolicyStage);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "/CustomerCommResponseAction")]
        string CustomerResponseAction(CustomerResponseAction CustomerResponse); 


    }
}
