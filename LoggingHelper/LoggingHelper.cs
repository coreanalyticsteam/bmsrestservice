﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LoggingHelper
{
    public class LoggingHelper
    {
        public static void LogIntoFile(string request, string response, string exception, string MethodName)
        {
            try
            {
                string strMessage = string.Empty;
                if (!string.IsNullOrEmpty(request))
                    strMessage = strMessage + "Request - " + request + Environment.NewLine;
                if (!string.IsNullOrEmpty(response))
                    strMessage = strMessage + "Response - " + response + Environment.NewLine;
                if (!string.IsNullOrEmpty(exception))
                    strMessage = strMessage + "Error - " + exception + Environment.NewLine;


                strMessage = strMessage + System.DateTime.Now.ToString();
                String FilePath = @"d:\servicelog";
                if (!String.IsNullOrEmpty(FilePath.Trim()))
                {
                    if (!Directory.Exists(FilePath))
                        Directory.CreateDirectory(FilePath);
                    string file = FilePath + @"\" + MethodName  + ".txt";

                    //if (!File.Exists(file))
                    //{
                    //    File.Create(file);
                    //    TextWriter tw = new StreamWriter(file);
                    //    tw.WriteLine(strMessage);
                    //    tw.Close();
                    //}
                    //else if (File.Exists(file))
                    //{
                    //    TextWriter tw = new StreamWriter(file);
                    //    tw.WriteLine(strMessage);
                    //    tw.Close();
                    //}
                    if (!File.Exists(file))
                        File.Create(file);

                    using (StreamWriter w = File.AppendText(file))
                    {
                        w.WriteLine(strMessage + Environment.NewLine);
                        w.Flush();
                        w.Close();
                    };
                    //System.IO.File.AppendAllText(file, strMessage + Environment.NewLine);

                    //TextWriter tw = new StreamWriter(file);
                    //tw.WriteLine(DateTime.Now + strMessage + Environment.NewLine);
                    //tw.Close();
                }
            }
            catch { }
        }    
    }
    
}
